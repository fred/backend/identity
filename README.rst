================
Identity backend
================

Identity backend based on gRPC API for `FRED <https://fred.nic.cz/>`_ registry.


How to build
============

.. code-block:: bash

   $ mkdir __build
   $ cd __build
   $ cmake -DAPI_IDENTITY_DIR=<path-to>/api/identity \
           -DLIBLOG_DIR=<path-to>/liblog \
           -DLIBPG_DIR=<path-to>/libpg \
           -DLIBSTRONG_DIR=<path-to>/libstrong \
           -DLIBDIAGNOSTICS_DIR=<path-to>/libdiagnostics \
           -DAPI_DIAGNOSTICS_DIR=<path-to>/api/diagnostics ..

   $ cmake --build . -j <jobs-num> --target all
