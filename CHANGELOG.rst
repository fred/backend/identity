CHANGELOG
=========

.. contents:: Releases
   :backlinks: none
   :local:

Unreleased
----------

2.3.0 (not released yet)
------------------------

* Do not allow contacts with the organization attribute filled in

2.1.0 (2022-09-19)
------------------

* Switch to ``libfred@6.1.1`` to support authinfo with ttl feature

2.0.0 (2021-12-19)
------------------

* Add ``libfred`` dependency to use common registry operations
  (replace standalone implementation by ``libfred`` usage where possible)
* Add registry contact update implementation
* Update CMake build

1.0.2 (2021-12-14)
------------------

* Fix update of contact blocking states

1.0.1 (2021-12-14)
------------------

* Fix update of contact blocking states
* Temporarily disable registry contact update

1.0.0 (2021-11-10)
------------------

* Initial version
