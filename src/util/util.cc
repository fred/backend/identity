/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/util/util.hh"

#include "src/cfg.hh"

#include "liblog/liblog.hh"
#include "libpg/detail/libpq_layer_impl.hh"

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/xpressive/xpressive.hpp>

#include <algorithm>
#include <chrono>
#include <utility>

namespace Fred {
namespace Identity {
namespace Util {

namespace {

struct DbAccess
{
    struct ReadOnly;
    struct ReadWrite;
};

template <typename Src, typename Dst>
void set_option(const boost::optional<Src>& src_main, const boost::optional<Src>& src_fallback, LibStrong::Optional<Dst>& dst)
{
    if (src_main != boost::none)
    {
        dst = Dst{*src_main};
    }
    else if (src_fallback != boost::none)
    {
        dst = Dst{*src_fallback};
    }
}

decltype(auto) make_dsn(const Cfg::Options::Database::Dsn& main_option, const Cfg::Options::Database::Dsn& fallback_option)
{
    LibPg::Dsn result;
    set_option(main_option.host, fallback_option.host, result.host);
    set_option(main_option.host_addr, fallback_option.host_addr, result.host_addr);
    set_option(main_option.port, fallback_option.port, result.port);
    set_option(main_option.user, fallback_option.user, result.user);
    set_option(main_option.dbname, fallback_option.dbname, result.db_name);
    set_option(main_option.password, fallback_option.password, result.password);
    set_option(main_option.connect_timeout, fallback_option.connect_timeout, result.connect_timeout);
    return result;
}

template <typename Access>
decltype(auto) make_dsn(const Cfg::Options::Database& db_option);

template <>
decltype(auto) make_dsn<DbAccess::ReadOnly>(const Cfg::Options::Database& db_options)
{
    return make_dsn(db_options.read_only, db_options.common);
}

template <>
decltype(auto) make_dsn<DbAccess::ReadWrite>(const Cfg::Options::Database& db_options)
{
    return make_dsn(db_options.read_write, db_options.common);
}

std::string serialize_connectdb_params(
        const char* const* keywords,
        const char* const* values)
{
    std::string result;
    while (*keywords != nullptr)
    {
        if (!result.empty())
        {
            result.append(" ");
        }
        static const std::string keyword_password = "password";
        const auto& keyword = *keywords;
        result.append(keyword).append(":");
        if (keyword == keyword_password)
        {
            result.append("******");
        }
        else
        {
            const auto& value = *values;
            result.append(value != nullptr ? value : "");
        }
        ++keywords;
        ++values;
    }
    return result;
}

std::string value_to_string(const char* value)
{
    if (value == nullptr)
    {
        return "NULL";
    }
    static constexpr auto quotation_mark = "`";
    return std::string{quotation_mark}.append(value).append(quotation_mark);
}

std::string serialize_exec_params(
        int n_params,
        const ::Oid* param_types,
        const char* const* param_values,
        const int* param_lengths,
        const int* param_formats)
{
    std::string result;
    const bool all_params_are_string = (param_types == nullptr) &&
                                       (param_lengths == nullptr) &&
                                       (param_formats == nullptr);
    if (all_params_are_string)
    {
        int param_idx = 1;
        std::for_each(
                param_values,
                param_values + n_params,
                [&](const char* value)
                {
                    if (!result.empty())
                    {
                        result.append(", ");
                    }
                    result.append("$" + std::to_string(param_idx) + ":");
                    result.append(value_to_string(value));
                    ++param_idx;
                });
    }
    else if (n_params == 0)
    {
        result = "no params";
    }
    else if (n_params == 1)
    {
        result = "one param";
    }
    else
    {
        result = std::to_string(n_params).append(" params");
    }
    return result;
}

void notice_handler(void*, const char* message)
{
    const auto message_length = std::strlen(message);
    if ((0 < message_length) && (message[message_length - 1] == '\n'))
    {
        LIBLOG_INFO("{}", std::string{message, message_length - 1});
    }
    else
    {
        LIBLOG_INFO("{}", message);
    }
}

std::string describe_rows_affected(::PGresult* result)
{
    const char* const rows_affected_str = const_cast<const char*>(::PQcmdTuples(result));
    const bool is_rows_affected_cmd = rows_affected_str[0] != '\0';
    if (!is_rows_affected_cmd)
    {
        return "";
    }
    try
    {
        const auto rows_affected = std::stoull(rows_affected_str);
        switch (rows_affected)
        {
            case 0:
                return "no row affected";
            case 1:
                return " 1 row affected";
        }
        return std::string{rows_affected_str} + " rows affected";
    }
    catch (...)
    {
        return "??? rows affected";
    }
}

std::string describe_cmd(::PGresult* result)
{
    const char* const status_str = const_cast<const char*>(::PQcmdStatus(result));
    const auto rows_affected = describe_rows_affected(result);
    if (rows_affected.empty())
    {
        return status_str;
    }
    return std::string{status_str} + " " + describe_rows_affected(result);
}

std::string describe_tuples(::PGresult* result)
{
    const auto tuples = ::PQntuples(result);
    switch (tuples)
    {
    case 0:
        return "returns no row";
    case 1:
        return "returns 1 row";
    }
    return "returns " + std::to_string(tuples) + " rows";
}

template <typename Duration>
std::string describe_result(::PGresult* result, const Duration& duration)
{
    const std::string duration_str = std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(duration).count());
    if (result == nullptr)
    {
        return "no result " + duration_str;
    }
    switch (::PQresultStatus(result))
    {
        case PGRES_EMPTY_QUERY:
            return "empty query done in " + duration_str + "ms";
        case PGRES_COMMAND_OK:
            return describe_cmd(result) + " done in " + duration_str + "ms";
        case PGRES_TUPLES_OK:
            return describe_tuples(result) + " in " + duration_str + "ms";
        default:
            return "bad result done in " + duration_str + "ms";
    }
}

class LibPqLogLayer : public LibPg::Detail::LibPqLayerSimpleImplementation
{
public:
    ~LibPqLogLayer() override { }
private:
    class Timer
    {
    public:
        Timer()
            : t0_{std::chrono::steady_clock::now()}
        { }
        decltype(auto) duration() const
        {
            return std::chrono::steady_clock::now() - t0_;
        }
    private:
        const std::chrono::steady_clock::time_point t0_;
    };

    using Base = LibPg::Detail::LibPqLayerSimpleImplementation;
    ::PGconn* PQconnectdbParams(
            const char* const* keywords,
            const char* const* values,
            int expand_dbname) const override
    {
        thread_local int count = 0;
        if (expand_dbname == 0)
        {
            LIBLOG_DEBUG("[{}] \"{}\"", count, serialize_connectdb_params(keywords, values));
        }
        else
        {
            LIBLOG_DEBUG("[{}] \"{}\", expand_dbname = {}", count, serialize_connectdb_params(keywords, values), expand_dbname);
        }
        const Timer timer;
        auto* const result = this->Base::PQconnectdbParams(keywords, values, expand_dbname);
        LIBLOG_DEBUG("[{}] result = ({}) in {}ms", count, static_cast<void*>(result), std::chrono::duration_cast<std::chrono::milliseconds>(timer.duration()).count());
        this->PQsetNoticeProcessor(result, notice_handler, nullptr);
        ++count;
        return result;
    }

    ::PGresult* PQexec(::PGconn* conn, const char* query) const override
    {
        thread_local int count = 0;
        LIBLOG_DEBUG("[{}] \"{}\"", count, query);
        const Timer timer;
        auto* const result = this->Base::PQexec(conn, query);
        LIBLOG_DEBUG("[{}] {}", count, describe_result(result, timer.duration()));
        ++count;
        return result;
    }

    ::PGresult* PQexecParams(
            ::PGconn* conn,
            const char* command,
            int nParams,
            const ::Oid* paramTypes,
            const char* const* paramValues,
            const int* paramLengths,
            const int* paramFormats,
            int resultFormat) const override
    {
        thread_local int count = 0;
        if (resultFormat == 0)
        {
            LIBLOG_DEBUG("[{}] \"{}\", [{}]", count, command, serialize_exec_params(nParams, paramTypes, paramValues, paramLengths, paramFormats));
        }
        else
        {
            LIBLOG_DEBUG("[{}] \"{}\", [{}], resultFormat = {}", count, command, serialize_exec_params(nParams, paramTypes, paramValues, paramLengths, paramFormats), resultFormat);
        }
        const Timer timer;
        auto* const result = this->Base::PQexecParams(conn, command, nParams, paramTypes, paramValues, paramLengths, paramFormats, resultFormat);
        LIBLOG_DEBUG("[{}] {}", count, describe_result(result, timer.duration()));
        ++count;
        return result;
    }

    void PQfinish(::PGconn* conn) const override
    {
        LIBLOG_DEBUG("({})", static_cast<void*>(conn));
        this->Base::PQfinish(conn);
    }
};

template <typename Access>
LibPg::PgConnection get_connection()
{
    static const class LogLayerInstaller
    {
    public:
        LogLayerInstaller()
            : log_layer_{new LibPqLogLayer{}}
        {
            LibPg::set_libpq_layer(log_layer_);
        }
    private:
        LibPqLogLayer* log_layer_;
    } log_layer_installer{};
    static const auto dsn = make_dsn<Access>(Cfg::Options::get().database);
    return LibPg::PgConnection{dsn};
}

}//namespace Fred::Identity::Util::{anonymous}

LibPg::PgConnection make_ro_connection()
{
    return get_connection<DbAccess::ReadOnly>();
}

LibPg::PgConnection make_rw_connection()
{
    return get_connection<DbAccess::ReadWrite>();
}

template <typename Level>
LibPg::PgRoTransaction make_ro_transaction(LibPg::PgTransaction::IsolationLevel<Level> level)
{
    return LibPg::PgRoTransaction{make_ro_connection(), level};
}

LibPg::PgRwTransaction make_rw_transaction()
{
    return LibPg::PgRwTransaction{make_rw_connection()};
}

template LibPg::PgRoTransaction make_ro_transaction<>(LibPg::PgTransaction::IsolationLevel<SessionDefault>);
template LibPg::PgRoTransaction make_ro_transaction<SerializableDeferrable>(LibPg::PgTransaction::IsolationLevel<SerializableDeferrable>);

/////////////////////////////////////////////////////////////////////////////////////////////
// function birthdate_from_string_to_date was copied from the server project so be lenient //
/////////////////////////////////////////////////////////////////////////////////////////////

//try recognize birth date in input string and convert to boost date
boost::gregorian::date birthdate_from_string_to_date(const std::string& birthdate)
{
    using namespace boost::xpressive;

    try
    {

    static sregex re_space = sregex::compile("\\s+");//spaces regex

    //current year, check birthdate in future
    int cy = boost::posix_time::second_clock::local_time().date().year();


    std::string birthdatenospaces
        = regex_replace(birthdate, re_space,std::string(""));//ignore spaces

    smatch match_result;//output of regex_match

    //try yyyy-mm-dd or yyyy/mm/dd
    static sregex re_date_yyyy_mm_dd
        = sregex::compile("^(\\d{4})([/-])(\\d{1,2})\\2(\\d{1,2})$");
    if(regex_match( birthdatenospaces, match_result, re_date_yyyy_mm_dd ))
    {
        return boost::gregorian::from_string(match_result[0]);
    }//if re_date_yyyy_mm_dd

    //try yyyymmdd or ddmmyyyy
    static sregex re_date_dddddddd
        = sregex::compile("^(\\d{2})(\\d{2})(\\d{2})(\\d{2})$");
    if(regex_match( birthdatenospaces, match_result, re_date_dddddddd ))
    {

        int v1_yyyy = boost::lexical_cast<int>(
                    std::string(match_result[1]) + std::string(match_result[2]));
        int v1_mm = boost::lexical_cast<int>(match_result[3]);
        int v1_dd = boost::lexical_cast<int>(match_result[4]);

        if ((v1_yyyy >=1900) && (v1_yyyy <=cy)
                && (v1_mm >= 1) && (v1_mm <= 12)
                && (v1_dd >= 1) && (v1_dd <= 31))
        {
            return boost::gregorian::from_undelimited_string(match_result[0]);
        }

        int v2_yyyy = boost::lexical_cast<int>(
                    std::string(match_result[3]) + std::string(match_result[4]));
        int v2_mm = boost::lexical_cast<int>(match_result[2]);
        int v2_dd = boost::lexical_cast<int>(match_result[1]);

        if ((v2_yyyy >=1900) && (v2_yyyy <=cy)
                && (v2_mm >= 1) && (v2_mm <= 12)
                && (v2_dd >= 1) && (v2_dd <= 31))
        {
            return boost::gregorian::from_undelimited_string(
                std::string(match_result[3])+std::string(match_result[4])+std::string(match_result[2])+std::string(match_result[1])
                );
        }

        throw std::runtime_error("birthdate error: invalid birthdate dddddddd");

    }//if re_date_dddddddd

    //try ddmyyyy
    static sregex re_date_ddddddd
        = sregex::compile("^(\\d{2})(\\d{1})(\\d{4})$");
    if(regex_match( birthdatenospaces, match_result, re_date_ddddddd ))
    {

        int yyyy = boost::lexical_cast<int>(std::string(match_result[3]));
        int mm = boost::lexical_cast<int>(match_result[2]);
        int dd = boost::lexical_cast<int>(match_result[1]);

        if ((yyyy >=1900) && (yyyy <=cy)
                && (mm >= 1) && (mm <= 12)
                && (dd >= 1) && (dd <= 31))
        {
            return boost::gregorian::from_undelimited_string(
                std::string(match_result[3])+"0"+std::string(match_result[2])+std::string(match_result[1])
                );
        }

        throw std::runtime_error("birthdate error: invalid birthdate ddddddd");

    }//if re_date_ddddddd

    //try dd.mm.yyyy or dd/mm/yyyy or dd-mm-yyyy
    static sregex re_date_dd_mm_yyyy
        = sregex::compile("^(\\d{1,2})([\\./-])(\\d{1,2})\\2(\\d{4})$");
    if(regex_match( birthdatenospaces, match_result, re_date_dd_mm_yyyy ))
    {
        int yyyy = boost::lexical_cast<int>(match_result[4]);
        int mm = boost::lexical_cast<int>(match_result[3]);
        int dd = boost::lexical_cast<int>(match_result[1]);

        if ((yyyy >=1900) && (yyyy <=cy)
                && (mm >= 1) && (mm <= 12)
                && (dd >= 1) && (dd <= 31))
        {
            return boost::gregorian::from_undelimited_string(
            str(boost::format("%|04d|%|02d|%|02d|")
            % match_result[4]
            % match_result[3]
            % match_result[1])
            );
        }

        throw std::runtime_error("birthdate error: invalid birthdate dd_mm_yyyy");

    }//if re_date_dd_mm_yyyy

    //try yymmdd or ddmmyy
    static sregex re_date_dddddd
        = sregex::compile("^(\\d{2})(\\d{2})(\\d{2})$");
    if(regex_match( birthdatenospaces, match_result, re_date_dddddd ))
    {
        //yymmdd
        int yy = boost::lexical_cast<int>(match_result[1]);
        int mm = boost::lexical_cast<int>(match_result[2]);
        int dd = boost::lexical_cast<int>(match_result[3]);

        //current year
        int cy1 = cy/100;
        int cy2 = cy%100;

        if(yy > cy2 )
        {
            yy = yy + ((cy1 - 1) * 100);
        }
        else
        {//yy<= cy2
            yy = yy + (cy1 * 100);
        }

        if ((yy >=1900) && (yy <=2010)//for legacy data only
                && (mm >= 1) && (mm <= 12)
                && (dd >= 1) && (dd <= 31))
        {
            return boost::gregorian::from_undelimited_string(
                str(boost::format("%|04d|%|02d|%|02d|")
                % yy % mm % dd)
                );
        }

        //ddmmyy
        yy = boost::lexical_cast<int>(match_result[3]);
        mm = boost::lexical_cast<int>(match_result[2]);
        dd = boost::lexical_cast<int>(match_result[1]);

        if(yy > cy2 )
        {
            yy = yy + ((cy1 - 1) * 100);
        }
        else
        {//yy<= cy2
            yy = yy + (cy1 * 100);
        }

        if ((yy >=1900) && (yy <=2010)//for legacy data only
                && (mm >= 1) && (mm <= 12)
                && (dd >= 1) && (dd <= 31))
        {
            return boost::gregorian::from_undelimited_string(
                str(boost::format("%|04d|%|02d|%|02d|")
                % yy % mm % dd)
                );
        }

        throw std::runtime_error("birthdate error: invalid birthdate dddddd");

    }//if re_date_dddddd


    //try yy_mm_dd or dd_mm_yy
    static sregex re_date_dd_dd_dd
        = sregex::compile("^(\\d{1,2})([\\./-])(\\d{1,2})\\2(\\d{1,2})$");
    if(regex_match( birthdatenospaces, match_result, re_date_dd_dd_dd ))
    {
        //yymmdd
        int yy = boost::lexical_cast<int>(match_result[1]);
        int mm = boost::lexical_cast<int>(match_result[3]);
        int dd = boost::lexical_cast<int>(match_result[4]);

        //current year
        int cy1 = cy/100;
        int cy2 = cy%100;

        if(yy > cy2 )
        {
            yy = yy + ((cy1 - 1) * 100);
        }
        else
        {//yy<= cy2
            yy = yy + (cy1 * 100);
        }

        if ((yy >=1900) && (yy <=2010)//for legacy data only
                && (mm >= 1) && (mm <= 12)
                && (dd >= 1) && (dd <= 31))
        {
            return boost::gregorian::from_undelimited_string(
                str(boost::format("%|04d|%|02d|%|02d|")
                % yy % mm % dd)
                );
        }

        //dd_mm_yy
        yy = boost::lexical_cast<int>(match_result[4]);
        mm = boost::lexical_cast<int>(match_result[3]);
        dd = boost::lexical_cast<int>(match_result[1]);

        if(yy > cy2 )
        {
            yy = yy + ((cy1 - 1) * 100);
        }
        else
        {//yy<= cy2
            yy = yy + (cy1 * 100);
        }

        if ((yy >=1900) && (yy <=2010)//for legacy data only
                && (mm >= 1) && (mm <= 12)
                && (dd >= 1) && (dd <= 31))
        {
            return boost::gregorian::from_undelimited_string(
                str(boost::format("%|04d|%|02d|%|02d|")
                % yy % mm % dd)
                );
        }

        throw std::runtime_error("birthdate error: invalid birthdate dd_dd_dd");

    }//if re_date_dd_dd_dd

    //try yyyymmdd000000
    return  boost::gregorian::from_undelimited_string(birthdatenospaces);

    }//try
    catch(std::exception&)
    {
        return boost::gregorian::date();
    }
}

}//namespace Fred::Identity::Util
}//namespace Fred::Identity
}//namespace Fred
