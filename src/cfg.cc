/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "src/cfg.hh"

#include <boost/program_options.hpp>

#include <syslog.h>

#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#ifndef DEFAULT_CONFIG_FILE
#define DEFAULT_CONFIG_FILE "identity.conf"
#endif

namespace Cfg {

namespace {

template <typename E>
[[noreturn]] void raise(std::string msg)
{
    struct ExceptionImpl : E
    {
        explicit ExceptionImpl(std::string msg) : msg{std::move(msg)} { }
        const char* what() const noexcept override { return msg.c_str(); }
        std::string msg;
    };
    throw ExceptionImpl{std::move(msg)};
}

//critical/error/warning/info/debug/trace
decltype(auto) make_severity(const std::string& str)
{
    static const std::unordered_map<std::string, LibLog::Level> str2level =
    {
        { "critical", LibLog::Level::critical },
        { "error", LibLog::Level::error },
        { "warning", LibLog::Level::warning },
        { "info", LibLog::Level::info },
        { "debug", LibLog::Level::debug },
        { "trace", LibLog::Level::trace }
    };
    const auto level_itr = str2level.find(str);
    if (level_itr != str2level.end())
    {
        return level_itr->second;
    }
    raise<Exception>("\"" + str + "\" not a valid severity");
}

constexpr int fake_argc = -1;
constexpr const char* const fake_argv[] = { nullptr };

}//namespace Cfg::{anonymous}

const Options& Options::get()
{
    return init(fake_argc, fake_argv);
}

const Options& Options::init(int argc, const char* const* argv)
{
    static const Options* singleton_ptr = nullptr;
    const bool init_requested = (argc != fake_argc) || (argv != fake_argv);
    const bool first_run = singleton_ptr == nullptr;
    if (first_run)
    {
        if (!init_requested)
        {
            raise<Exception>("First call of Cfg::Options::init must contain valid arguments");
        }
        static const Options singleton{argc, argv};
        singleton_ptr = &singleton;
    }
    else if (init_requested)
    {
        raise<Exception>("Only first call of Cfg::Options::init can contain valid arguments");
    }
    return *singleton_ptr;
}

namespace {

constexpr struct OptionNames
{
    static constexpr struct Identity
    {
        static constexpr auto listen = "identity.listen";
        static constexpr auto service = "identity.service";
    } identity = {};
} option{};

static constexpr unsigned terminal_width = 120;

decltype(auto) make_generic_options(boost::optional<std::string>& config_file_name)
{
    boost::program_options::options_description generic_options{"Generic options", terminal_width};
    const auto set_config_file_name = [&](const std::string& file_name) { config_file_name = file_name; };
    generic_options.add_options()
        ("help,h", "produce help message")
        ("version,V", "display version information")
        ("config,c",
#ifdef DEFAULT_CONFIG_FILE
         boost::program_options::value<std::string>()->default_value(std::string{DEFAULT_CONFIG_FILE})->notifier(set_config_file_name),
#else
         boost::program_options::value<std::string>()->notifier(set_config_file_name),
#endif
         "name of a file of a configuration.");
    return generic_options;
}

decltype(auto) make_database_common_options(Options::Database::Dsn& dsn)
{
    boost::program_options::options_description database_options{"Common database access options", terminal_width};
    const auto set_host = [&](const std::string& host) { dsn.host = host; };
    const auto set_host_addr = [&](const std::string& ip_address) { dsn.host_addr = boost::asio::ip::address::from_string(ip_address); };
    const auto set_port = [&](::in_port_t port) { dsn.port = port; };
    const auto set_user = [&](const std::string& user) { dsn.user = user; };
    const auto set_dbname = [&](const std::string& dbname) { dsn.dbname = dbname; };
    const auto set_password = [&](const std::string& password) { dsn.password = password; };
    const auto set_connect_timeout = [&](std::chrono::seconds::rep seconds) { dsn.connect_timeout = std::chrono::seconds{seconds}; };
    database_options.add_options()
        ("database.host", boost::program_options::value<std::string>()->notifier(set_host), "name of host to connect to")
        ("database.host_addr", boost::program_options::value<std::string>()->notifier(set_host_addr), "IP address of host to connect to")
        ("database.port", boost::program_options::value<::in_port_t>()->notifier(set_port), "port number to connect to at the server host")
        ("database.user", boost::program_options::value<std::string>()->notifier(set_user), "PostgreSQL user name to connect as")
        ("database.dbname", boost::program_options::value<std::string>()->notifier(set_dbname), "the database name")
        ("database.password", boost::program_options::value<std::string>()->notifier(set_password), "password used for password authentication")
        ("database.connect_timeout",
         boost::program_options::value<std::chrono::seconds::rep>()->notifier(set_connect_timeout),
         "Maximum wait for connection, in seconds. Zero or not specified means wait indefinitely. "
         "It is not recommended to use a timeout of less than 2 seconds.");
    return database_options;
}

decltype(auto) make_ro_database_options(Options::Database::Dsn& dsn)
{
    boost::program_options::options_description database_options{"Read-only database access options", terminal_width};
    const auto set_host = [&](const std::string& host) { dsn.host = host; };
    const auto set_host_addr = [&](const std::string& ip_address) { dsn.host_addr = boost::asio::ip::address::from_string(ip_address); };
    const auto set_port = [&](::in_port_t port) { dsn.port = port; };
    const auto set_user = [&](const std::string& user) { dsn.user = user; };
    const auto set_dbname = [&](const std::string& dbname) { dsn.dbname = dbname; };
    const auto set_password = [&](const std::string& password) { dsn.password = password; };
    const auto set_connect_timeout = [&](std::chrono::seconds::rep seconds) { dsn.connect_timeout = std::chrono::seconds{seconds}; };
    database_options.add_options()
        ("database.ro.host", boost::program_options::value<std::string>()->notifier(set_host), "name of host to connect to")
        ("database.ro.host_addr", boost::program_options::value<std::string>()->notifier(set_host_addr), "IP address of host to connect to")
        ("database.ro.port", boost::program_options::value<::in_port_t>()->notifier(set_port), "port number to connect to at the server host")
        ("database.ro.user", boost::program_options::value<std::string>()->notifier(set_user), "PostgreSQL user name to connect as")
        ("database.ro.dbname", boost::program_options::value<std::string>()->notifier(set_dbname), "the database name")
        ("database.ro.password", boost::program_options::value<std::string>()->notifier(set_password), "password used for password authentication")
        ("database.ro.connect_timeout",
         boost::program_options::value<std::chrono::seconds::rep>()->notifier(set_connect_timeout),
         "Maximum wait for connection, in seconds. Zero or not specified means wait indefinitely. "
         "It is not recommended to use a timeout of less than 2 seconds.");
    return database_options;
}

decltype(auto) make_rw_database_options(Options::Database::Dsn& dsn)
{
    boost::program_options::options_description database_options{"Read/write database access options", terminal_width};
    const auto set_host = [&](const std::string& host) { dsn.host = host; };
    const auto set_host_addr = [&](const std::string& ip_address) { dsn.host_addr = boost::asio::ip::address::from_string(ip_address); };
    const auto set_port = [&](::in_port_t port) { dsn.port = port; };
    const auto set_user = [&](const std::string& user) { dsn.user = user; };
    const auto set_dbname = [&](const std::string& dbname) { dsn.dbname = dbname; };
    const auto set_password = [&](const std::string& password) { dsn.password = password; };
    const auto set_connect_timeout = [&](std::chrono::seconds::rep seconds) { dsn.connect_timeout = std::chrono::seconds{seconds}; };
    database_options.add_options()
        ("database.rw.host", boost::program_options::value<std::string>()->notifier(set_host), "name of host to connect to")
        ("database.rw.host_addr", boost::program_options::value<std::string>()->notifier(set_host_addr), "IP address of host to connect to")
        ("database.rw.port", boost::program_options::value<::in_port_t>()->notifier(set_port), "port number to connect to at the server host")
        ("database.rw.user", boost::program_options::value<std::string>()->notifier(set_user), "PostgreSQL user name to connect as")
        ("database.rw.dbname", boost::program_options::value<std::string>()->notifier(set_dbname), "the database name")
        ("database.rw.password", boost::program_options::value<std::string>()->notifier(set_password), "password used for password authentication")
        ("database.rw.connect_timeout",
         boost::program_options::value<std::chrono::seconds::rep>()->notifier(set_connect_timeout),
         "Maximum wait for connection, in seconds. Zero or not specified means wait indefinitely. "
         "It is not recommended to use a timeout of less than 2 seconds.");
    return database_options;
}

struct CommonLogOptions
{
    enum class Device
    {
        console,
        file,
        syslog
    };
    static decltype(auto) make_device(const std::string& device)
    {
        static const std::unordered_map<std::string, Device> str2device =
            {
                { "console", Device::console },
                { "file", Device::file },
                { "syslog", Device::syslog }
            };
        const auto device_itr = str2device.find(device);
        if (device_itr != str2device.end())
        {
            return device_itr->second;
        }
        raise<Exception>("\"" + device + "\" is not valid device");
    }
    std::set<Device> devices;
    boost::optional<LibLog::Level> min_severity;
    boost::optional<bool> log_grpc_library_events;
    void set_devices(const std::vector<std::string>& str_devices)
    {
        std::for_each(
            str_devices.begin(),
            str_devices.end(),
            [&](const std::string& str_device)
            {
                devices.insert(make_device(str_device));
            });
    }
    void set_min_severity(const std::string& severity)
    {
        if (min_severity != boost::none)
        {
            raise<Exception>("severity can be defined at most once");
        }
        min_severity = make_severity(severity);
    }
    void set_log_grpc_library_events(bool value)
    {
        log_grpc_library_events = value;
    }
};

decltype(auto) make_log_options(CommonLogOptions& common_log_options)
{
    boost::program_options::options_description options_description{"Logging options", terminal_width};
    const auto set_devices = [&](const std::vector<std::string>& devices) { common_log_options.set_devices(devices); };
    const auto set_min_severity = [&](const std::string& severity) { common_log_options.set_min_severity(severity); };
    const auto set_log_grpc_library_events = [&](bool to_log) { common_log_options.set_log_grpc_library_events(to_log); };
    options_description.add_options()
        ("log.device", boost::program_options::value<std::vector<std::string>>()->multitoken()->notifier(set_devices), "where to log (console/file/syslog)")
        ("log.min_severity",
         boost::program_options::value<std::string>()->notifier(set_min_severity),
         "do not log more trivial events; "
         "severity in descending order: critical/error/warning/info/debug/trace")
        ("log.grpc_library",
         boost::program_options::value<bool>()->zero_tokens()->notifier(set_log_grpc_library_events),
         "log gRPC library events");
    return options_description;
}

//stderr/stdout
decltype(auto) make_output_stream(const std::string& str)
{
    static const std::unordered_map<std::string, LibLog::Sink::ConsoleSinkConfig::OutputStream> str2output_stream =
        {
            { "stderr", LibLog::Sink::ConsoleSinkConfig::OutputStream::stderr },
            { "stdout", LibLog::Sink::ConsoleSinkConfig::OutputStream::stdout }
        };
    const auto output_stream_itr = str2output_stream.find(str);
    if (output_stream_itr != str2output_stream.end())
    {
        return output_stream_itr->second;
    }
    raise<Exception>("\"" + str + "\" not a valid output stream");
}

//always/auto/never
decltype(auto) make_color_mode(const std::string& str)
{
    static const std::unordered_map<std::string, LibLog::ColorMode> str2color_mode =
        {
            { "always", LibLog::ColorMode::always },
            { "auto", LibLog::ColorMode::automatic },
            { "never", LibLog::ColorMode::never }
        };
    const auto color_mode_itr = str2color_mode.find(str);
    if (color_mode_itr != str2color_mode.end())
    {
        return color_mode_itr->second;
    }
    raise<Exception>("\"" + str + "\" not a valid color mode");
}

struct ConsoleLogOptions
{
    boost::optional<LibLog::Level> min_severity;
    boost::optional<LibLog::Sink::ConsoleSinkConfig::OutputStream> output_stream;
    boost::optional<LibLog::ColorMode> color_mode;
    void set_min_severity(const std::string& value)
    {
        if (min_severity != boost::none)
        {
            raise<Exception>("severity can be defined at most once");
        }
        min_severity = make_severity(value);
    }
    void set_output_stream(const std::string& value)
    {
        if (output_stream != boost::none)
        {
            raise<Exception>("severity can be defined at most once");
        }
        output_stream = make_output_stream(value);
    }
    void set_color_mode(const std::string& value)
    {
        if (color_mode != boost::none)
        {
            raise<Exception>("color mode can be defined at most once");
        }
        color_mode = make_color_mode(value);
    }
};

decltype(auto) make_log_console_options(ConsoleLogOptions& console_log_options)
{
    boost::program_options::options_description options_description{"Logging on console options", terminal_width};
    const auto set_min_severity = [&](const std::string& severity) { console_log_options.set_min_severity(severity); };
    const auto set_output_stream = [&](const std::string& output_stream) { console_log_options.set_output_stream(output_stream); };
    const auto set_color_mode = [&](const std::string& color_mode) { console_log_options.set_color_mode(color_mode); };
    options_description.add_options()
        ("log.console.min_severity",
         boost::program_options::value<std::string>()->notifier(set_min_severity),
         "do not log more trivial events; "
         "severity in descending order: critical/error/warning/info/debug/trace")
        ("log.console.output_stream",
         boost::program_options::value<std::string>()->notifier(set_output_stream),
         "where to log (stderr/stdout), default is stderr")
        ("log.console.color_mode",
         boost::program_options::value<std::string>()->notifier(set_color_mode),
         "how to colorize output (always/auto/never), default is never");
    return options_description;
}

struct FileLogOptions
{
    boost::optional<std::string> file_name;
    boost::optional<LibLog::Level> min_severity;
    void set_file_name(const std::string& name)
    {
        if (file_name != boost::none)
        {
            raise<Exception>("file name can be defined at most once");
        }
        file_name = name;
    }
    void set_min_severity(const std::string& severity)
    {
        if (min_severity != boost::none)
        {
            raise<Exception>("severity can be defined at most once");
        }
        min_severity = make_severity(severity);
    }
};

decltype(auto) make_log_file_options(FileLogOptions& file_log_options)
{
    boost::program_options::options_description options_description{"Logging into file options", terminal_width};
    const auto set_file_name = [&](const std::string& file_name) { file_log_options.set_file_name(file_name); };
    const auto set_min_severity = [&](const std::string& severity) { file_log_options.set_min_severity(severity); };
    options_description.add_options()
        ("log.file.file_name", boost::program_options::value<std::string>()->notifier(set_file_name), "what file to log into")
        ("log.file.min_severity",
         boost::program_options::value<std::string>()->notifier(set_min_severity),
         "do not log more trivial events; "
         "severity in descending order: critical/error/warning/info/debug/trace");
    return options_description;
}

struct SyslogLogOptions
{
    boost::optional<std::string> ident;
    boost::optional<int> facility_local_offset;
    boost::optional<LibLog::Level> min_severity;
    void set_ident(const std::string& value)
    {
        if (ident != boost::none)
        {
            raise<Exception>("ident can be defined at most once");
        }
        ident = value;
    }
    void set_facility(int value)
    {
        if ((value < 0) || (7 < value))
        {
            raise<Exception>("facility out of range [0, 7]");
        }
        if (facility_local_offset != boost::none)
        {
            raise<Exception>("facility can be defined at most once");
        }
        facility_local_offset = value;
    }
    void set_min_severity(const std::string& severity)
    {
        if (min_severity != boost::none)
        {
            raise<Exception>("severity can be defined at most once");
        }
        min_severity = make_severity(severity);
    }
};

decltype(auto) make_log_syslog_options(SyslogLogOptions& syslog_log_options)
{
    boost::program_options::options_description options_description{"Logging into syslog options", terminal_width};
    const auto set_ident = [&](const std::string& ident) { syslog_log_options.set_ident(ident); };
    const auto set_facility = [&](int facility) { syslog_log_options.set_facility(facility); };
    const auto set_min_severity = [&](const std::string& severity) { syslog_log_options.set_min_severity(severity); };
    options_description.add_options()
        ("log.syslog.ident", boost::program_options::value<std::string>()->notifier(set_ident), "what ident to log with (default is empty string)")
        ("log.syslog.facility",
         boost::program_options::value<int>()->notifier(set_facility),
         "what LOG_LOCALx facility to log with (x in range 0..7, default means facility LOG_USER)")
        ("log.syslog.min_severity",
         boost::program_options::value<std::string>()->notifier(set_min_severity),
         "do not log more trivial events; "
         "severity in descending order: critical/error/warning/info/debug/trace");
    return options_description;
}

decltype(auto) make_fred_options(Options::Fred& fred)
{
    boost::program_options::options_description fred_options{"FRED options", terminal_width};
    const auto set_registrar_originator = [&](const std::string& registrar) { fred.registrar_originator = registrar; };
    fred_options.add_options()
        ("fred.registrar_originator", boost::program_options::value<std::string>()->notifier(set_registrar_originator), "which registrar does modifying operations");
    return fred_options;
}

decltype(auto) make_server_options(Options::Server& server)
{
    boost::program_options::options_description server_options{"Server options", terminal_width};
    const auto notify_service = [&](const std::vector<std::string>& services)
    {
        static std::unordered_map<std::string, Options::Server::Service> str2service =
        {
            { "IdentityUser", Options::Server::Service::identity_user },
            { "IdentityManager", Options::Server::Service::identity_manager },
            { "IdentityChecker", Options::Server::Service::identity_checker },
            { "Diagnostics", Options::Server::Service::diagnostics },
        };
        std::transform(std::begin(services), std::end(services), std::inserter(server.services, server.services.begin()),
            [](const std::string& str_service)
            {
                const auto service_itr = str2service.find(str_service);
                if (service_itr == str2service.end())
                {
                    raise<Exception>(std::string{"\""} + str_service + "\" is invalid value of " + option.identity.service + " option");
                }
                return service_itr->second;
            });
    };
    server_options.add_options()
        (option.identity.listen, boost::program_options::value<std::string>(&server.listen_on), "server's listen address.")
        (option.identity.service,
         boost::program_options::value<std::vector<std::string>>()->multitoken()->notifier(notify_service),
         "gRPC service to run (possible values: IdentityUser, IdentityManager, IdentityChecker).");
    return server_options;
}

void collect_log_options(
    const CommonLogOptions& common_log_options,
    const ConsoleLogOptions& console_log_options,
    const FileLogOptions& file_log_options,
    const SyslogLogOptions& syslog_log_options,
    Options::Log& log_options)
{
    log_options.log_grpc_library_events = (common_log_options.log_grpc_library_events != boost::none) && *common_log_options.log_grpc_library_events;

    static constexpr auto default_log_min_severity = LibLog::Level::critical;
    const auto common_log_min_severity = common_log_options.min_severity != boost::none ? *common_log_options.min_severity
                                                                                        : default_log_min_severity;
    std::for_each(
        common_log_options.devices.begin(),
        common_log_options.devices.end(),
        [&](CommonLogOptions::Device device)
        {
            switch (device)
            {
            case CommonLogOptions::Device::console:
            {
                Options::Log::Console console;
                console.min_severity = console_log_options.min_severity != boost::none ? *console_log_options.min_severity
                                                                                       : common_log_min_severity;
                static constexpr auto default_output_stream = LibLog::Sink::ConsoleSinkConfig::OutputStream::stderr;
                console.output_stream = console_log_options.output_stream != boost::none ? *console_log_options.output_stream
                                                                                         : default_output_stream;
                static constexpr auto default_color_mode = LibLog::ColorMode::never;
                console.color_mode = console_log_options.color_mode != boost::none ? *console_log_options.color_mode
                                                                                   : default_color_mode;
                log_options.console = console;
                return;
            }
            case CommonLogOptions::Device::file:
            {
                if (file_log_options.file_name == boost::none)
                {
                    raise<MissingOption>("missing option: 'log.file.file_name'");
                }
                Options::Log::File log_file;
                log_file.file_name = *file_log_options.file_name;
                log_file.min_severity = file_log_options.min_severity != boost::none ? *file_log_options.min_severity
                                                                                     : common_log_min_severity;
                log_options.file = log_file;
                return;
            }
            case CommonLogOptions::Device::syslog:
            {
                Options::Log::Syslog syslog;
                static const std::string default_ident = "";
                syslog.ident = syslog_log_options.ident != boost::none ? *syslog_log_options.ident
                                                                       : default_ident;
                static constexpr int default_facility = LOG_USER;
                static const auto make_facility = [](int offset)
                {
                    switch (offset)
                    {
                        case 0: return LOG_LOCAL0;
                        case 1: return LOG_LOCAL1;
                        case 2: return LOG_LOCAL2;
                        case 3: return LOG_LOCAL3;
                        case 4: return LOG_LOCAL4;
                        case 5: return LOG_LOCAL5;
                        case 6: return LOG_LOCAL6;
                        case 7: return LOG_LOCAL7;
                    }
                    return default_facility;
                };
                syslog.facility = syslog_log_options.facility_local_offset != boost::none ? make_facility(*syslog_log_options.facility_local_offset)
                                                                                          : default_facility;
                syslog.min_severity = syslog_log_options.min_severity != boost::none ? *syslog_log_options.min_severity
                                                                                     : common_log_min_severity;
                static constexpr int default_options = LOG_CONS | LOG_ODELAY;
                syslog.options = default_options;
                log_options.syslog = syslog;
                return;
            }
            }
            raise<Exception>("Invalid device");
        });
}

}//namespace Cfg::{anonymous}

Options::Options(int argc, const char* const* argv)
{
    CommonLogOptions common_log_options;
    ConsoleLogOptions console_log_options;
    FileLogOptions file_log_options;
    SyslogLogOptions syslog_log_options;
    const boost::program_options::options_description generic_options = make_generic_options(config_file_name);
    const boost::program_options::options_description database_common_options = make_database_common_options(database.common);
    const boost::program_options::options_description ro_database_options = make_ro_database_options(database.read_only);
    const boost::program_options::options_description rw_database_options = make_rw_database_options(database.read_write);
    const boost::program_options::options_description log_options = make_log_options(common_log_options);
    const boost::program_options::options_description log_console_options = make_log_console_options(console_log_options);
    const boost::program_options::options_description log_file_options = make_log_file_options(file_log_options);
    const boost::program_options::options_description log_syslog_options = make_log_syslog_options(syslog_log_options);
    const boost::program_options::options_description fred_options = make_fred_options(fred);
    const boost::program_options::options_description server_options = make_server_options(identity);

    boost::program_options::options_description command_line_options{"fred-identity-services options", terminal_width};
    command_line_options
        .add(generic_options)
        .add(log_options)
        .add(log_console_options)
        .add(log_file_options)
        .add(log_syslog_options)
        .add(database_common_options)
        .add(ro_database_options)
        .add(rw_database_options)
        .add(fred_options)
        .add(server_options);
    boost::program_options::options_description config_file_options;
    config_file_options
        .add(log_options)
        .add(log_console_options)
        .add(log_file_options)
        .add(log_syslog_options)
        .add(database_common_options)
        .add(ro_database_options)
        .add(rw_database_options)
        .add(fred_options)
        .add(server_options);

    boost::program_options::variables_map variables_map;
    const auto has_option = [&](const char* option) { return 0 < variables_map.count(option); };
    try
    {
        boost::program_options::store(
                boost::program_options::command_line_parser(argc, argv)
                    .options(command_line_options)
                    .run(),
                variables_map);
    }
    catch (const boost::program_options::unknown_option& unknown_option)
    {
        std::ostringstream out;
        out << unknown_option.what() << "\n\n" << command_line_options;
        raise<UnknownOption>(out.str());
    }
    boost::program_options::notify(variables_map);

    if (has_option("help"))
    {
        std::ostringstream out;
        out << command_line_options;
        raise<AllDone>(out.str());
    }
    if (has_option("version"))
    {
        std::ostringstream out;
        out << "Server Version: " << PACKAGE_VERSION << std::endl;
        out << "Identity API Version: " << API_IDENTITY_VERSION << std::endl;
        out << "Diagnostics API Version: " << API_DIAGNOSTICS_VERSION << std::endl;
        out << "LibDiagnostics Version: " << LIBDIAGNOSTICS_VERSION << std::endl;
        out << "LibLog Version: " << LIBLOG_VERSION << std::endl;
        out << "LibPg Version: " << LIBPG_VERSION << std::endl;
        raise<AllDone>(out.str());
    }
    const bool config_file_name_presents = config_file_name != boost::none;
    if (config_file_name_presents)
    {
        std::ifstream config_file{*config_file_name};
        if (!config_file)
        {
            raise<Exception>("can not open config file \"" + *config_file_name + "\"");
        }
        try
        {
            boost::program_options::store(
                    boost::program_options::parse_config_file(
                            config_file,
                            config_file_options),
                    variables_map);
        }
        catch (const boost::program_options::unknown_option& unknown_option)
        {
            std::ostringstream out;
            out << unknown_option.what() << "\n\n" << command_line_options;
            raise<UnknownOption>(out.str());
        }
        boost::program_options::notify(variables_map);
    }

    collect_log_options(
        common_log_options,
        console_log_options,
        file_log_options,
        syslog_log_options,
        log);

    const auto required = [&](const char* variable)
    {
        if (variables_map.count(variable) == 0)
        {
            raise<MissingOption>("missing option: '" + std::string{variable} + "'");
        }
    };

    required(option.identity.listen);
    required(option.identity.service);
}

void handle_cli_args(int argc, char** argv)
{
    try
    {
        Cfg::Options::init(argc, argv);
    }
    catch (const Cfg::AllDone& all_done)
    {
        std::cout << all_done.what() << std::endl;
        ::exit(EXIT_SUCCESS);
    }
    catch (const Cfg::UnknownOption& unknown_option)
    {
        std::cerr << unknown_option.what() << std::endl;
        ::exit(EXIT_FAILURE);
    }
    catch (const Cfg::MissingOption& missing_option)
    {
        std::cerr << missing_option.what() << std::endl;
        ::exit(EXIT_FAILURE);
    }
    catch (const Cfg::Exception& e)
    {
        std::cerr << "Configuration problem: " << e.what() << std::endl;
        ::exit(EXIT_FAILURE);
    }
    catch (const std::exception& e)
    {
        std::cerr << "Exception caught: " << e.what() << std::endl;
        ::exit(EXIT_FAILURE);
    }
    catch (...)
    {
        std::cerr << "Unexpected exception caught" << std::endl;
        ::exit(EXIT_FAILURE);
    }
}

}//namespace Cfg
