/*
 * Copyright (C) 2021-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/iface/tools/tools.hh"

#include "fred_api/identity/service_identity_checker_grpc.grpc.pb.h"
#include "fred_api/identity/service_identity_manager_grpc.grpc.pb.h"

#include "liblog/liblog.hh"

namespace Fred {
namespace Identity {
namespace Iface {
namespace Tools {

grpc::Status on_internal_server_error(const std::exception& e)
{
    LIBLOG_ERROR("InternalServerError caught: {}", e.what());
    return grpc::Status(grpc::StatusCode::UNKNOWN, "InternalServerError");
}

grpc::Status on_invalid_argument(const std::exception& e)
{
    LIBLOG_WARNING("InvalidArgument caught: {}", e.what());
    return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "InvalidArgument", e.what());
}

grpc::Status on_not_found(const std::exception& e)
{
    LIBLOG_WARNING("NotFound caught: {}", e.what());
    return grpc::Status(grpc::StatusCode::NOT_FOUND, "NotFound", e.what());
}

grpc::Status on_already_exists(const std::exception& e)
{
    LIBLOG_WARNING("AlreadyExists caught: {}", e.what());
    return grpc::Status(grpc::StatusCode::ALREADY_EXISTS, "AlreadyExists", e.what());
}

grpc::Status on_failed_precondition(const std::exception& e)
{
    LIBLOG_WARNING("FailedPrecondition caught: {}", e.what());
    return grpc::Status(grpc::StatusCode::FAILED_PRECONDITION, "FailedPrecondition", e.what());
}

grpc::Status on_sql_error(const std::exception& e)
{
    LIBLOG_WARNING("SQL error: {}", e.what());
    return grpc::Status(grpc::StatusCode::UNKNOWN, "SQL error", e.what());
}

grpc::Status on_std_exception(const std::exception& e)
{
    LIBLOG_ERROR("std::exception caught: {}", e.what());
    return grpc::Status(grpc::StatusCode::UNKNOWN, "Unknown exception based on std::exception");
}

grpc::Status on_unknown_exception()
{
    LIBLOG_ERROR("unknown exception caught");
    return grpc::Status(grpc::StatusCode::UNKNOWN, "Unknown exception");
}

template <typename T>
grpc::Status on_contact_does_not_exist(const std::exception& e, T* response)
{
    LIBLOG_INFO("Contact does not exist: {}", e.what());
    response->Clear();
    response->mutable_exception()->mutable_contact_does_not_exist();
    return grpc::Status::OK;
}

template <typename T>
grpc::Status on_history_id_mismatch(const std::exception& e, T* response)
{
    LIBLOG_INFO("HistoryId mismatch: {}", e.what());
    response->Clear();
    response->mutable_exception()->mutable_history_id_mismatch();
    return grpc::Status::OK;
}

template <typename T>
grpc::Status on_identity_attached(const std::exception& e, T* response)
{
    LIBLOG_INFO("Identity attached: {}", e.what());
    response->Clear();
    response->mutable_exception()->mutable_identity_attached();
    return grpc::Status::OK;
}

template <typename T>
grpc::Status on_identity_does_not_exist(const std::exception& e, T* response)
{
    LIBLOG_INFO("Identity does not exist: {}", e.what());
    response->Clear();
    response->mutable_exception()->mutable_identity_does_not_exist();
    return grpc::Status::OK;
}

template <typename T>
grpc::Status on_missing_contact_id(const std::exception& e, T* response)
{
    LIBLOG_INFO("Parameter contact_id is missing: {}", e.what());
    response->Clear();
    response->mutable_exception()->mutable_missing_contact_id();
    return grpc::Status::OK;
}

template <typename T>
grpc::Status on_contact_is_organization(const std::exception& e, T* response)
{
    LIBLOG_INFO("Contact is an organization: {}", e.what());
    response->Clear();
    response->mutable_exception()->mutable_contact_is_organization();
    return grpc::Status::OK;
}

template grpc::Status on_contact_does_not_exist(const std::exception&, Api::AttachIdentityReply*);
template grpc::Status on_history_id_mismatch(const std::exception&, Api::AttachIdentityReply*);
template grpc::Status on_identity_attached(const std::exception&, Api::AttachIdentityReply*);
template grpc::Status on_contact_is_organization(const std::exception&, Api::AttachIdentityReply*);

template grpc::Status on_contact_does_not_exist(const std::exception&, Api::DetachIdentityReply*);
template grpc::Status on_identity_does_not_exist(const std::exception&, Api::DetachIdentityReply*);

template grpc::Status on_contact_does_not_exist(const std::exception&, Api::GetContactIdByHandleReply*);

template grpc::Status on_contact_does_not_exist(const std::exception&, Api::CheckCurrentContactDataReply*);
template grpc::Status on_history_id_mismatch(const std::exception&, Api::CheckCurrentContactDataReply*);
template grpc::Status on_missing_contact_id(const std::exception&, Api::CheckCurrentContactDataReply*);
template grpc::Status on_contact_is_organization(const std::exception&, Api::CheckCurrentContactDataReply*);

}//namespace Fred::Identity::Iface::Tools
}//namespace Fred::Identity::Iface
}//namespace Fred::Identity
}//namespace Fred
