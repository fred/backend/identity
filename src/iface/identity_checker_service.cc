/*
 * Copyright (C) 2021-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/iface/identity_checker_service.hh"
#include "src/iface/tools/tools.hh"
#include "src/impl/exceptions.hh"
#include "src/impl/lib/check_current_contact_data.hh"
#include "src/impl/unwrapper/check_current_contact_data_request.hh"
#include "src/util/util.hh"

#include "liblog/liblog.hh"

#include "libpg/pg_exception.hh"

namespace Fred {
namespace Identity {

namespace Iface {

grpc::Status IdentityCheckerService::check_current_contact_data(
        grpc::ServerContext*,
        const Api::CheckCurrentContactDataRequest* request,
        Api::CheckCurrentContactDataReply* response)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, "IdentityChecker", __func__);
    try
    {
        auto tx = Util::make_rw_transaction();
        Impl::Lib::check_current_contact_data(tx, Impl::Unwrapper::CheckCurrentContactDataRequest{*request}, response);
        commit(std::move(tx));
        return grpc::Status::OK;
    }
    catch (const Impl::ContactDoesNotExist& e)
    {
        return Tools::on_contact_does_not_exist(e, response);
    }
    catch (const Impl::HistoryIdMismatch& e)
    {
        return Tools::on_history_id_mismatch(e, response);
    }
    catch (const Impl::MissingContactId& e)
    {
        return Tools::on_missing_contact_id(e, response);
    }
    catch (const Impl::ContactIsOrganization& e)
    {
        return Tools::on_contact_is_organization(e, response);
    }
    catch (const Impl::InvalidArgument& e)
    {
        return Tools::on_invalid_argument(e);
    }
    catch (const LibPg::ExecFailure& e)
    {
        return Tools::on_sql_error(e);
    }
    catch (const std::exception& e)
    {
        return Tools::on_std_exception(e);
    }
    catch (...)
    {
        return Tools::on_unknown_exception();
    }
}

}//namespace Fred::Identity::Iface
}//namespace Fred::Identity
}//namespace Fred
