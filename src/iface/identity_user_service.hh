/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IDENTITY_USER_SERVICE_HH_95A8172CF12C4253D63800AEF1485CBD//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define IDENTITY_USER_SERVICE_HH_95A8172CF12C4253D63800AEF1485CBD

#include "fred_api/identity/service_identity_user_grpc.grpc.pb.h"

namespace Fred {
namespace Identity {
namespace Iface {

class IdentityUserService final : public Api::IdentityUser::Service
{
private:
    ::grpc::Status get_contacts_by_identity(
            ::grpc::ServerContext* context,
            const Api::GetContactsByIdentityRequest* request,
            Api::GetContactsByIdentityReply* response) override;
};

}//namespace Fred::Identity::Iface
}//namespace Fred::Identity
}//namespace Fred

#endif//IDENTITY_USER_SERVICE_HH_95A8172CF12C4253D63800AEF1485CBD
