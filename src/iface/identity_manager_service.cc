/*
 * Copyright (C) 2021-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/iface/identity_manager_service.hh"
#include "src/iface/tools/tools.hh"
#include "src/impl/exceptions.hh"
#include "src/impl/lib/attach_identity.hh"
#include "src/impl/lib/detach_identity.hh"
#include "src/impl/lib/get_contact_id_by_handle.hh"
#include "src/impl/lib/update_identity.hh"
#include "src/util/util.hh"

#include "liblog/liblog.hh"

#include "libpg/pg_exception.hh"

namespace Fred {
namespace Identity {
namespace Iface {

::grpc::Status IdentityManagerService::attach_identity(
        ::grpc::ServerContext*,
        const Api::AttachIdentityRequest* request,
        Api::AttachIdentityReply* response)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, "IdentityManager", __func__);
    try
    {
        auto tx = Util::make_rw_transaction();
        Impl::Lib::attach_identity(tx, Impl::Unwrapper::AttachIdentityRequest{*request});
        commit(std::move(tx));
        return grpc::Status::OK;
    }
    catch (const Impl::ContactDoesNotExist& e)
    {
        return Tools::on_contact_does_not_exist(e, response);
    }
    catch (const Impl::HistoryIdMismatch& e)
    {
        return Tools::on_history_id_mismatch(e, response);
    }
    catch (const Impl::IdentityAttached& e)
    {
        return Tools::on_identity_attached(e, response);
    }
    catch (const Impl::ContactIsOrganization& e)
    {
        return Tools::on_contact_is_organization(e, response);
    }
    catch (const LibPg::ExecFailure& e)
    {
        return Tools::on_sql_error(e);
    }
    catch (const std::exception& e)
    {
        return Tools::on_std_exception(e);
    }
    catch (...)
    {
        return Tools::on_unknown_exception();
    }
}

grpc::Status IdentityManagerService::update_identity(
        grpc::ServerContext*,
        const Api::UpdateIdentityRequest* request,
        google::protobuf::Empty*)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, "IdentityManager", __func__);
    try
    {
        auto tx = Util::make_rw_transaction();
        Impl::Lib::update_identity(tx, Impl::Unwrapper::UpdateIdentityRequest{*request});
        commit(std::move(tx));
        return grpc::Status::OK;
    }
    catch (const LibPg::ExecFailure& e)
    {
        return Tools::on_sql_error(e);
    }
    catch (const std::exception& e)
    {
        return Tools::on_std_exception(e);
    }
    catch (...)
    {
        return Tools::on_unknown_exception();
    }
}

::grpc::Status IdentityManagerService::detach_identity(::grpc::ServerContext*, const Api::DetachIdentityRequest* request, Api::DetachIdentityReply* response)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, "IdentityManager", __func__);
    try
    {
        auto tx = Util::make_rw_transaction();
        Impl::Lib::detach_identity(tx, Impl::Unwrapper::DetachIdentityRequest{*request});
        commit(std::move(tx));
        return grpc::Status::OK;
    }
    catch (const Impl::ContactDoesNotExist& e)
    {
        return Tools::on_contact_does_not_exist(e, response);
    }
    catch (const Impl::IdentityDoesNotExist& e)
    {
        return Tools::on_identity_does_not_exist(e, response);
    }
    catch (const LibPg::ExecFailure& e)
    {
        return Tools::on_sql_error(e);
    }
    catch (const std::exception& e)
    {
        return Tools::on_std_exception(e);
    }
    catch (...)
    {
        return Tools::on_unknown_exception();
    }
}

::grpc::Status IdentityManagerService::get_contact_id_by_handle(::grpc::ServerContext*, const Api::GetContactIdByHandleRequest* request, Api::GetContactIdByHandleReply* response)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, "IdentityManager", __func__);
    try
    {
        auto tx = Util::make_ro_transaction();
        Impl::Lib::get_contact_id_by_handle(tx, Impl::Unwrapper::GetContactIdByHandleRequest{*request}, response);
        commit(std::move(tx));
        return grpc::Status::OK;
    }
    catch (const Impl::ContactDoesNotExist& e)
    {
        return Tools::on_contact_does_not_exist(e, response);
    }
    catch (const LibPg::ExecFailure& e)
    {
        return Tools::on_sql_error(e);
    }
    catch (const std::exception& e)
    {
        return Tools::on_std_exception(e);
    }
    catch (...)
    {
        return Tools::on_unknown_exception();
    }
}

}//namespace Fred::Identity::Iface
}//namespace Fred::Identity
}//namespace Fred
