/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IDENTITY_CHECKER_SERVICE_HH_425F3EB14B691FDF51316458F06DF942//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define IDENTITY_CHECKER_SERVICE_HH_425F3EB14B691FDF51316458F06DF942

#include "fred_api/identity/service_identity_checker_grpc.grpc.pb.h"

namespace Fred {
namespace Identity {
namespace Iface {

class IdentityCheckerService final : public Api::IdentityChecker::Service
{
private:
    grpc::Status check_current_contact_data(
            grpc::ServerContext* context,
            const Api::CheckCurrentContactDataRequest* request,
            Api::CheckCurrentContactDataReply* response) override;
};

}//namespace Fred::Identity::Iface
}//namespace Fred::Identity
}//namespace Fred

#endif//IDENTITY_CHECKER_SERVICE_HH_425F3EB14B691FDF51316458F06DF942
