/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IDENTITY_MANAGER_SERVICE_HH_06DCDA9505B45CD7F394683CBC0FB985//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define IDENTITY_MANAGER_SERVICE_HH_06DCDA9505B45CD7F394683CBC0FB985

#include "fred_api/identity/service_identity_manager_grpc.grpc.pb.h"

namespace Fred {
namespace Identity {
namespace Iface {

class IdentityManagerService final : public Api::IdentityManager::Service
{
private:
    grpc::Status attach_identity(
            grpc::ServerContext* context,
            const Api::AttachIdentityRequest* request,
            Api::AttachIdentityReply* response) override;
    grpc::Status update_identity(
            grpc::ServerContext* context,
            const Api::UpdateIdentityRequest* request,
            google::protobuf::Empty* response) override;
    grpc::Status detach_identity(
            grpc::ServerContext* context,
            const Api::DetachIdentityRequest* request,
            Api::DetachIdentityReply* response) override;
    grpc::Status get_contact_id_by_handle(
            grpc::ServerContext* context,
            const Api::GetContactIdByHandleRequest* request,
            Api::GetContactIdByHandleReply* response) override;
};

}//namespace Fred::Identity::Iface
}//namespace Fred::Identity
}//namespace Fred

#endif//IDENTITY_MANAGER_SERVICE_HH_06DCDA9505B45CD7F394683CBC0FB985
