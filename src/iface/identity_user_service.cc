/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/iface/identity_user_service.hh"
#include "src/iface/tools/tools.hh"

#include "src/util/util.hh"

#include "src/impl/lib/get_contacts_by_identity.hh"
#include "src/impl/exceptions.hh"

#include "liblog/liblog.hh"

#include "libpg/pg_exception.hh"

#include <algorithm>
#include <cmath>

namespace Fred {
namespace Identity {
namespace Iface {

grpc::Status IdentityUserService::get_contacts_by_identity(
        ::grpc::ServerContext*,
        const Api::GetContactsByIdentityRequest* request,
        Api::GetContactsByIdentityReply* response)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, "IdentityUser", __func__);
    try
    {
        auto tx = Util::make_ro_transaction();
        Impl::Lib::get_contacts_by_identity(tx, Impl::Unwrapper::GetContactsByIdentityRequest{*request}, Impl::Wrapper::GetContactsByIdentityReply{response});
        commit(std::move(tx));
        return grpc::Status::OK;
    }
    catch (const Impl::InvalidArgument& e)
    {
        return Tools::on_invalid_argument(e);
    }
    catch (const LibPg::ExecFailure& e)
    {
        return Tools::on_sql_error(e);
    }
    catch (const std::exception& e)
    {
        return Tools::on_std_exception(e);
    }
    catch (...)
    {
        return Tools::on_unknown_exception();
    }
}

}//namespace Fred::Identity::Iface
}//namespace Fred::Identity
}//namespace Fred
