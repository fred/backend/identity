/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "src/cfg.hh"
#include "src/util/util.hh"

#include "src/iface/identity_checker_service.hh"
#include "src/iface/identity_manager_service.hh"
#include "src/iface/identity_user_service.hh"

#include "libdiagnostics/diagnostics.hh"

#include "liblog/liblog.hh"
#include "liblog/log_config.hh"
#include "liblog/log.hh"

#include "libpg/pg_rw_transaction.hh"

#include <grpc++/grpc++.h>
#include <grpc/support/log.h>

#include <unistd.h>

#include <cerrno>
#include <csignal>
#include <cstdlib>
#include <cstring>

#include <algorithm>
#include <chrono>
#include <iostream>
#include <memory>
#include <string>

namespace Fred {
namespace Identity {

namespace {

template <typename Service>
constexpr decltype(auto) service_name();

template <>
constexpr decltype(auto) service_name<Iface::IdentityCheckerService>()
{
    return "IdentityChecker";
}

template <>
constexpr decltype(auto) service_name<Iface::IdentityManagerService>()
{
    return "IdentityManager";
}

template <>
constexpr decltype(auto) service_name<Iface::IdentityUserService>()
{
    return "IdentityUser";
}

template <typename Service>
void register_service(std::unique_ptr<Service>& service, grpc::ServerBuilder& builder)
{
    LIBLOG_INFO("{}", service_name<Service>());
    service = std::make_unique<Service>();
    builder.RegisterService(service.get());
}

enum class RequestedAction
{
    exit,
    restart
};

namespace Diagnostics {

LibDiagnostics::AboutReply about()
{
    return {
            Fred::LibDiagnostics::AboutReply::ServerVersion{PACKAGE_VERSION},
            {
                {Fred::LibDiagnostics::ServiceName{service_name<Iface::IdentityCheckerService>()}, Fred::LibDiagnostics::AboutReply::ApiVersion{API_IDENTITY_VERSION}},
                {Fred::LibDiagnostics::ServiceName{service_name<Iface::IdentityManagerService>()}, Fred::LibDiagnostics::AboutReply::ApiVersion{API_IDENTITY_VERSION}},
                {Fred::LibDiagnostics::ServiceName{service_name<Iface::IdentityUserService>()}, Fred::LibDiagnostics::AboutReply::ApiVersion{API_IDENTITY_VERSION}}
            }};
}

template <typename Service>
LibDiagnostics::StatusReply::Service status_of_configured_service();

template <>
LibDiagnostics::StatusReply::Service status_of_configured_service<Iface::IdentityManagerService>()
{
    bool db_ro_accessible = false;
    try
    {
        auto ro_transaction = Util::make_ro_transaction();
        const auto query = "SELECT 'status'::TEXT WHERE false";
        exec(ro_transaction, query);
        commit(std::move(ro_transaction));
        db_ro_accessible = true;
    }
    catch (...) { }

    bool db_rw_accessible = false;
    try
    {
        constexpr const auto diagnostics_test_function_name = "diagnostics_status_test_2BE2A01F723149AC898A495BD4BC33F8";
        const auto query =
                // FIXME schema sql injection :) ``?
                "CREATE OR REPLACE FUNCTION " + std::string{diagnostics_test_function_name} + "() "
                "RETURNS void "
                "LANGUAGE sql "
                "AS $$$$; "
                "DROP FUNCTION " + diagnostics_test_function_name;

        auto rw_transaction = Util::make_rw_transaction();
        exec(rw_transaction, query);
        commit(std::move(rw_transaction));
        db_rw_accessible = true;
    }
    catch (...) { }

    const LibDiagnostics::StatusReply::Extras no_extras{};

    if (db_ro_accessible && db_rw_accessible)
    {
        return LibDiagnostics::StatusReply::Service{
                LibDiagnostics::StatusReply::Status::ok,
                LibDiagnostics::StatusReply::Note{"OK"},
                no_extras};
    }
    if (db_ro_accessible && !db_rw_accessible)
    {
        return LibDiagnostics::StatusReply::Service{
                LibDiagnostics::StatusReply::Status::error,
                LibDiagnostics::StatusReply::Note{"Database not accessible (R/W)."},
                no_extras};
    }
    if (db_rw_accessible && !db_ro_accessible)
    {
        return LibDiagnostics::StatusReply::Service{
                LibDiagnostics::StatusReply::Status::error,
                LibDiagnostics::StatusReply::Note{"Database not accessible (R/O)."},
                no_extras};
    }
    return LibDiagnostics::StatusReply::Service{
            LibDiagnostics::StatusReply::Status::error,
            LibDiagnostics::StatusReply::Note{"Database not accessible."},
            no_extras};
}

template <>
LibDiagnostics::StatusReply::Service status_of_configured_service<Iface::IdentityUserService>()
{
    bool db_ro_accessible = false;
    try
    {
        auto ro_transaction = Util::make_ro_transaction();
        const auto query = "SELECT 'status'::TEXT WHERE false";
        exec(ro_transaction, query);
        commit(std::move(ro_transaction));
        db_ro_accessible = true;
    }
    catch (...) { }

    const LibDiagnostics::StatusReply::Extras no_extras{};

    return db_ro_accessible
                   ? LibDiagnostics::StatusReply::Service{
                       LibDiagnostics::StatusReply::Status::ok,
                       LibDiagnostics::StatusReply::Note{"OK"},
                       no_extras}
                   : LibDiagnostics::StatusReply::Service{
                       LibDiagnostics::StatusReply::Status::error,
                       LibDiagnostics::StatusReply::Note{"Database not accessible."},
                       no_extras};
}

template <>
LibDiagnostics::StatusReply::Service status_of_configured_service<Iface::IdentityCheckerService>()
{
    return status_of_configured_service<Iface::IdentityUserService>();
}

LibDiagnostics::StatusReply status()
{
    LibDiagnostics::StatusReply reply;

    std::for_each(std::begin(Cfg::Options::get().identity.services), std::end(Cfg::Options::get().identity.services),
        [&](Cfg::Options::Server::Service service)
        {
            switch (service)
            {
                case Cfg::Options::Server::Service::identity_checker:
                    reply.services[service_name<Iface::IdentityCheckerService>()] = status_of_configured_service<Iface::IdentityCheckerService>();
                    return;
                case Cfg::Options::Server::Service::identity_manager:
                    reply.services[service_name<Iface::IdentityManagerService>()] = status_of_configured_service<Iface::IdentityManagerService>();
                    return;
                case Cfg::Options::Server::Service::identity_user:
                    reply.services[service_name<Iface::IdentityUserService>()] = status_of_configured_service<Iface::IdentityUserService>();
                    return;
                case Cfg::Options::Server::Service::diagnostics:
                    // managed by libdiagnostics library
                    return;
            }
            throw std::runtime_error{"unsupported service"};
        });
    return reply;
}

}//namespace Fred::Identity::{anonymous}::Diagnostics

decltype(auto) run_server(const Cfg::Options::Server& options)
{
    LIBLOG_SET_CONTEXT(Ctx, log_ctx, __func__);

    ::sigset_t blocked_signals;

    // Other threads created by grpc::Server will inherit a copy of the signal mask.
    ::sigemptyset(&blocked_signals);
    ::sigaddset(&blocked_signals, SIGHUP);
    ::sigaddset(&blocked_signals, SIGINT);
    ::sigaddset(&blocked_signals, SIGQUIT);
    ::sigaddset(&blocked_signals, SIGTERM);
    const auto pthread_sigmask_result = ::pthread_sigmask(SIG_BLOCK, &blocked_signals, nullptr);
    constexpr int operation_success = 0;
    if (pthread_sigmask_result != operation_success)
    {
        throw std::runtime_error(std::strerror(pthread_sigmask_result));
    }

    std::unique_ptr<Iface::IdentityCheckerService> identity_checker_service;
    std::unique_ptr<Iface::IdentityManagerService> identity_manager_service;
    std::unique_ptr<Iface::IdentityUserService> identity_user_service;
    auto diagnostics_service =
            LibDiagnostics::make_grpc_service(
                    Fred::Identity::Diagnostics::about,
                    Fred::Identity::Diagnostics::status);

    grpc::ServerBuilder builder;

    // Listen on the given address without any authentication mechanism.
    builder.AddListeningPort(options.listen_on, grpc::InsecureServerCredentials());

    // Register "service" as the instance through which we'll communicate with
    // clients. In this case it corresponds to a *synchronous* service.
    std::for_each(std::begin(options.services), std::end(options.services),
        [&](Cfg::Options::Server::Service service)
        {
            switch (service)
            {
                case Cfg::Options::Server::Service::identity_checker:
                    register_service(identity_checker_service, builder);
                    return;
                case Cfg::Options::Server::Service::identity_manager:
                    register_service(identity_manager_service, builder);
                    return;
                case Cfg::Options::Server::Service::identity_user:
                    register_service(identity_user_service, builder);
                    return;
                case Cfg::Options::Server::Service::diagnostics:
                    builder.RegisterService(diagnostics_service.get());
                    return;
            }
            throw std::runtime_error{"unsupported service"};
        });

    // Finally assemble the server.
    const std::unique_ptr<grpc::Server> server(builder.BuildAndStart());

    int received_signal;
    const auto sigwait_result = ::sigwait(&blocked_signals, &received_signal);
    if (sigwait_result != operation_success)
    {
        throw std::runtime_error(std::strerror(sigwait_result));
    }
    auto requested_action = RequestedAction::exit;
    switch (received_signal)
    {
        case SIGHUP:
            LIBLOG_DEBUG("SIGHUP received");
            requested_action = RequestedAction::restart;
            break;
        case SIGINT:
            LIBLOG_DEBUG("SIGINT received");
            break;
        case SIGQUIT:
            LIBLOG_DEBUG("SIGQUIT received");
            break;
        case SIGTERM:
            LIBLOG_DEBUG("SIGTERM received");
            break;
        default:
            throw std::runtime_error("unexpected signal received");
    }
    static constexpr auto finish_timeout = std::chrono::seconds(10);
    const std::chrono::system_clock::time_point deadline = std::chrono::system_clock::now() + finish_timeout;
    server->Shutdown(deadline);
    return requested_action;
}

}//namespace Fred::Identity::{anonymous}

}//namespace Fred::Identity
}//namespace Fred

namespace {

void disable_grpc_library_logging()
{
    static const auto dummy_log_func = [](::gpr_log_func_args*) { };
    ::gpr_set_log_function(dummy_log_func);
    static constexpr auto do_not_log = static_cast<::gpr_log_severity>(GPR_LOG_SEVERITY_ERROR + 1);
    ::gpr_set_log_verbosity(do_not_log);
}

void grpc_log(::gpr_log_func_args* log_args)
{
    try
    {
        switch (log_args->severity)
        {
            case GPR_LOG_SEVERITY_DEBUG:
                LIBLOG_DEBUG("{} at {}:{}", log_args->message, log_args->file, log_args->line);
                return;
            case GPR_LOG_SEVERITY_INFO:
                LIBLOG_INFO("{} at {}:{}", log_args->message, log_args->file, log_args->line);
                return;
            case GPR_LOG_SEVERITY_ERROR:
                LIBLOG_ERROR("{} at {}:{}", log_args->message, log_args->file, log_args->line);
                return;
        }
        LIBLOG_WARNING("{} at {}:{}", log_args->message, log_args->file, log_args->line);
    } catch (...)
    { }
}

constexpr auto no_log_grpc_severity = static_cast<::gpr_log_severity>(GPR_LOG_SEVERITY_ERROR + 1);

constexpr ::gpr_log_severity make_grpc_log_severity(LibLog::Level min_severity)
{
    switch (min_severity)
    {
        case LibLog::Level::critical:
            return no_log_grpc_severity;
        case LibLog::Level::error:
            return GPR_LOG_SEVERITY_ERROR;
        case LibLog::Level::warning:
        case LibLog::Level::info:
            return GPR_LOG_SEVERITY_INFO;
        case LibLog::Level::debug:
        case LibLog::Level::trace:
            return GPR_LOG_SEVERITY_DEBUG;
    }
    return no_log_grpc_severity;
}

decltype(auto) get_min_severity(const Cfg::Options::Log& log_options)
{
    boost::optional<LibLog::Level> min_severity;
    const auto update_min_severity = [&](LibLog::Level severity)
    {
        if ((min_severity == boost::none) ||
            (severity < *min_severity))
        {
            min_severity = severity;
        }
    };
    if (log_options.console != boost::none)
    {
        update_min_severity(log_options.console->min_severity);
    }
    if (log_options.file != boost::none)
    {
        update_min_severity(log_options.file->min_severity);
    }
    if (log_options.syslog != boost::none)
    {
        update_min_severity(log_options.syslog->min_severity);
    }
    return min_severity;
}

void set_grpc_library_logging(const Cfg::Options::Log& log_options)
{
    if (!log_options.log_grpc_library_events)
    {
        LIBLOG_INFO("do not log gRPC library");
        disable_grpc_library_logging();
        return;
    }
    const auto min_severity = get_min_severity(log_options);
    if (min_severity == boost::none)
    {
        disable_grpc_library_logging();
        return;
    }
    ::gpr_set_log_function(grpc_log);
    ::gpr_set_log_verbosity(make_grpc_log_severity(*min_severity));
    LIBLOG_INFO("log gRPC library");
}

decltype(auto) make_sink_config(const Cfg::Options::Log::Console& log_options)
{
    LibLog::Sink::ConsoleSinkConfig sink_config;
    sink_config.set_level(log_options.min_severity);
    sink_config.set_output_stream(log_options.output_stream);
    sink_config.set_color_mode(log_options.color_mode);
    return sink_config;
}

decltype(auto) make_sink_config(const Cfg::Options::Log::File& log_options)
{
    LibLog::Sink::FileSinkConfig sink_config{log_options.file_name};
    sink_config.set_level(log_options.min_severity);
    return sink_config;
}

decltype(auto) make_sink_config(const Cfg::Options::Log::Syslog& log_options)
{
    LibLog::Sink::SyslogSinkConfig sink_config;
    sink_config.set_syslog_facility(log_options.facility);
    sink_config.set_level(log_options.min_severity);
    sink_config.set_syslog_ident(log_options.ident);
    sink_config.set_syslog_options(log_options.options);
    return sink_config;
}

void set_logging(const Cfg::Options::Log& log_options)
{
    bool has_device = false;
    LibLog::LogConfig log_config;
    if (log_options.console != boost::none)
    {
        try
        {
            log_config.add_sink_config(make_sink_config(*log_options.console));
            has_device = true;
        }
        catch (...) { }
    }
    if (log_options.file != boost::none)
    {
        try
        {
            log_config.add_sink_config(make_sink_config(*log_options.file));
            has_device = true;
        }
        catch (...) { }
    }
    if (log_options.syslog != boost::none)
    {
        try
        {
            log_config.add_sink_config(make_sink_config(*log_options.syslog));
            has_device = true;
        }
        catch (...) { }
    }
    if (has_device)
    {
        try
        {
            LibLog::Log::start<LibLog::ThreadMode::multi_threaded>(log_config);
            set_grpc_library_logging(log_options);
            return;
        }
        catch (const std::exception& e)
        {
            std::cerr << "Setting up logging failure: " << e.what() << std::endl;
            ::exit(EXIT_FAILURE);
        }
        catch (...)
        {
            std::cerr << "Setting up logging failure: Unexpected exception caught" << std::endl;
            ::exit(EXIT_FAILURE);
        }
    }
    try
    {
        disable_grpc_library_logging();
    }
    catch (...)
    {
        std::cerr << "unable to disable gRPC library logging" << std::endl;
    }
    std::cout << "no logging is configured!?" << std::endl;
}

decltype(auto) store_working_directory_if(bool to_store)
{
    static const auto free_char = [](char* ptr)
    {
        ::free(static_cast<void*>(ptr));
    };
    return std::unique_ptr<char[], decltype(free_char)>{to_store ? ::get_current_dir_name() : nullptr, free_char};
}

char* make_absolute_path_cmd(const char* pwd, const char* cmd)
{
    const auto pwd_len = std::strlen(pwd);
    const auto cmd_len = std::strlen(cmd);
    const bool pwd_end_is_directory = (0 < pwd_len) && (pwd[pwd_len - 1] == '/');
    if (pwd_end_is_directory)
    {
        char* const absolute_path_cmd = new char[pwd_len + cmd_len + 1];
        std::memcpy(absolute_path_cmd, pwd, pwd_len);
        std::memcpy(absolute_path_cmd + pwd_len, cmd, cmd_len + 1);
        return absolute_path_cmd;
    }
    char* const absolute_path_cmd = new char[pwd_len + 1 + cmd_len + 1];
    std::memcpy(absolute_path_cmd, pwd, pwd_len);
    absolute_path_cmd[pwd_len] = '/';
    std::memcpy(absolute_path_cmd + pwd_len + 1, cmd, cmd_len + 1);
    return absolute_path_cmd;
}

void set_working_directory_to_root()
{
    static constexpr int success = 0;
    if (::chdir("/") != success)
    {
        const auto msg = std::strerror(errno);
        LIBLOG_WARNING("chdir(\"/\") failure: {}", msg);
        throw std::runtime_error{msg};
    }
}

decltype(auto) is_relative_path(const char* path)
{
    return path[0] != '/';
}

}//namespace::{anonymous}

int main(int argc, char** argv)
{
    const auto& cmd = argv[0];
    const auto working_directory = store_working_directory_if(is_relative_path(cmd));
    set_working_directory_to_root();

    Cfg::handle_cli_args(argc, argv);

    try
    {
        set_logging(Cfg::Options::get().log);
        LIBLOG_SET_CONTEXT(Ctx, log_ctx, __func__);
        if (Fred::Identity::run_server(Cfg::Options::get().identity) == Fred::Identity::RequestedAction::restart)
        {
            LIBLOG_INFO("Server restart");
            if (working_directory != nullptr)
            {
                argv[0] = make_absolute_path_cmd(working_directory.get(), cmd);
                LIBLOG_INFO("cmd = \"{}\"", argv[0]);
            }
            ::execv(argv[0], argv);
            const int c_errno = errno;
            LIBLOG_ERROR("unable to restart server: {}", std::strerror(c_errno));
            return EXIT_FAILURE;
        }
        LIBLOG_INFO("Server shutdown");
        return EXIT_SUCCESS;
    }
    catch (const std::exception& e)
    {
        std::cerr << "Exception caught: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (...)
    {
        std::cerr << "Unexpected exception caught" << std::endl;
        return EXIT_FAILURE;
    }
}
