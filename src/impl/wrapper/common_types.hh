/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef COMMON_TYPES_HH_09F02C7147C8895D06717C3AE02FA820//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define COMMON_TYPES_HH_09F02C7147C8895D06717C3AE02FA820

#include "src/impl/api_type_setter.hh"

#include <boost/uuid/uuid.hpp>

#include <cstdint>
#include <string>

namespace Fred {
namespace Identity {
namespace Impl {
namespace Wrapper {

class Uuid : private ApiTypeSetter<Uuid>
{
public:
    template <typename ApiType>
    explicit Uuid(ApiType* ptr) noexcept;
    const Uuid& set_value(const boost::uuids::uuid& value) const;
};

class ContactId : private ApiTypeSetter<ContactId>
{
public:
    template <typename ApiType>
    explicit ContactId(ApiType* ptr) noexcept;
    Uuid set_uuid() const;
    const ContactId& set_numeric_id(std::uint64_t numeric_id) const;
    const ContactId& set_handle(std::string handle) const;
};

class ContactHistoryId : private ApiTypeSetter<ContactHistoryId>
{
public:
    template <typename ApiType>
    explicit ContactHistoryId(ApiType* ptr) noexcept;
    Uuid set_uuid() const;
};

class Date : private ApiTypeSetter<Date>
{
public:
    template <typename ApiType>
    explicit Date(ApiType* ptr) noexcept;
    void set_year(int value) const;
    void set_month(int value) const;
    void set_day(int value) const;
};

class PlaceAddress : private ApiTypeSetter<PlaceAddress>
{
public:
    template <typename ApiType>
    explicit PlaceAddress(ApiType* ptr) noexcept;
    const PlaceAddress& set_street(const std::string* begin, const std::string* end) const;
    const PlaceAddress& set_city(const std::string& value) const;
    const PlaceAddress& set_state_or_province(const std::string& value) const;
    const PlaceAddress& set_postal_code(const std::string& value) const;
    const PlaceAddress& set_country_code(const std::string& value) const;
};

}//namespace Fred::Identity::Impl::Wrapper
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#endif//COMMON_TYPES_HH_09F02C7147C8895D06717C3AE02FA820
