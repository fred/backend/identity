/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CHECK_CURRENT_CONTACT_DATA_REPLY_HH_4B2308B347C1C48BB53168018A34A0B7//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define CHECK_CURRENT_CONTACT_DATA_REPLY_HH_4B2308B347C1C48BB53168018A34A0B7

#include "src/impl/wrapper/common_types.hh"
#include "src/impl/api_type_setter.hh"

namespace Fred {
namespace Identity {
namespace Impl {
namespace Wrapper {

class CheckCurrentContactDataReply : private ApiTypeSetter<CheckCurrentContactDataReply>
{
public:
    template <typename ApiType>
    explicit CheckCurrentContactDataReply(ApiType* ptr) noexcept;
    class Data : private ApiTypeSetter<Data>
    {
    public:
        template <typename ApiType>
        explicit Data(ApiType* ref) noexcept;
        ContactHistoryId set_contact_history_id() const;
        void set_auth_info_matches(bool value) const;
        void set_name(const std::string& value) const;
        PlaceAddress set_address() const;
        Date set_birthdate() const;
        void set_broken_birthdate(const std::string& value) const;
    };
    Data set_data() const;
};

}//namespace Fred::Identity::Impl::Wrapper
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#endif//CHECK_CURRENT_CONTACT_DATA_REPLY_HH_4B2308B347C1C48BB53168018A34A0B7
