/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/wrapper/check_current_contact_data_reply.hh"
#include "src/impl/get_value.hh"

#include "fred_api/identity/service_identity_checker_grpc.pb.h"

#include <type_traits>
#include <utility>

namespace Fred {
namespace Identity {
namespace Impl {
namespace Wrapper {

namespace {

using ApiWrappers = std::tuple<KeyValuePair<CheckCurrentContactDataReply, Api::CheckCurrentContactDataReply>,
                               KeyValuePair<CheckCurrentContactDataReply::Data, Api::CheckCurrentContactDataReply::Data>,
                               KeyValuePair<Uuid, Api::Uuid>,
                               KeyValuePair<ContactId, Api::ContactId>>;

template <typename WrapperType>
using GetApiT = typename GetValue<WrapperType, ApiWrappers>::Type;

}//namespace Fred::Identity::Impl::Wrapper::{anonymous}

}//namespace Fred::Identity::Impl::Wrapper
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#include "src/impl/api_type_setter_impl.hh" // must be included after GetApiT defining

namespace Fred {
namespace Identity {
namespace Impl {
namespace Wrapper {

template <>
CheckCurrentContactDataReply::Data::Data(Api::CheckCurrentContactDataReply::Data* ptr) noexcept
    : ApiData{ptr}
{ }

ContactHistoryId CheckCurrentContactDataReply::Data::set_contact_history_id() const
{
    return ContactHistoryId{this->ApiData::get<Api::CheckCurrentContactDataReply::Data>()->mutable_contact_history_id()};
}

void CheckCurrentContactDataReply::Data::set_auth_info_matches(bool value) const
{
    this->ApiData::get<Api::CheckCurrentContactDataReply::Data>()->set_auth_info_matches(value);
}

void CheckCurrentContactDataReply::Data::set_name(const std::string& value) const
{
    this->ApiData::get<Api::CheckCurrentContactDataReply::Data>()->set_name(value);
}

PlaceAddress CheckCurrentContactDataReply::Data::set_address() const
{
    return PlaceAddress{this->ApiData::get<Api::CheckCurrentContactDataReply::Data>()->mutable_address()};
}

Date CheckCurrentContactDataReply::Data::set_birthdate() const
{
    return Date{this->ApiData::get<Api::CheckCurrentContactDataReply::Data>()->mutable_birthdate()};
}

void CheckCurrentContactDataReply::Data::set_broken_birthdate(const std::string& value) const
{
    this->ApiData::get<Api::CheckCurrentContactDataReply::Data>()->set_broken_birthdate(value);
}

template <>
CheckCurrentContactDataReply::CheckCurrentContactDataReply(Api::CheckCurrentContactDataReply* ptr) noexcept
    : ApiData{ptr}
{ }

CheckCurrentContactDataReply::Data CheckCurrentContactDataReply::set_data() const
{
    return Data{this->ApiData::get<Api::CheckCurrentContactDataReply>()->mutable_data()};
}

}//namespace Fred::Identity::Impl::Wrapper
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred
