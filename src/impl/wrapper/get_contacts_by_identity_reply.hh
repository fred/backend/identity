/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_CONTACTS_BY_IDENTITY_REPLY_HH_8026911483D7178D77D36B11F9EE6415//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define GET_CONTACTS_BY_IDENTITY_REPLY_HH_8026911483D7178D77D36B11F9EE6415

#include "src/impl/wrapper/common_types.hh"
#include "src/impl/api_type_setter.hh"

namespace Fred {
namespace Identity {
namespace Impl {
namespace Wrapper {

class GetContactsByIdentityReply : private ApiTypeSetter<GetContactsByIdentityReply>
{
public:
    template <typename ApiType>
    explicit GetContactsByIdentityReply(ApiType* ptr) noexcept;
    class Data : private ApiTypeSetter<Data>
    {
    public:
        template <typename ApiType>
        explicit Data(ApiType* ref) noexcept;
        ApiRepeatedTypeSetter<ContactId> set_contact_ids() const;
    };
    Data set_data() const;
};

}//namespace Fred::Identity::Impl::Wrapper
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#endif//GET_CONTACTS_BY_IDENTITY_REPLY_HH_8026911483D7178D77D36B11F9EE6415
