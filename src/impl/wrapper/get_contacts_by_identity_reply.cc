/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/wrapper/get_contacts_by_identity_reply.hh"
#include "src/impl/get_value.hh"

#include "fred_api/identity/service_identity_user_grpc.pb.h"

#include <type_traits>
#include <utility>

namespace Fred {
namespace Identity {
namespace Impl {
namespace Wrapper {

namespace {

using ApiWrappers = std::tuple<KeyValuePair<GetContactsByIdentityReply, Api::GetContactsByIdentityReply>,
                               KeyValuePair<GetContactsByIdentityReply::Data, Api::GetContactsByIdentityReply::Data>,
                               KeyValuePair<ApiRepeatedTypeSetter<ContactId>, ::google::protobuf::RepeatedPtrField<Api::ContactId>>,
                               KeyValuePair<Uuid, Api::Uuid>,
                               KeyValuePair<ContactId, Api::ContactId>>;

template <typename WrapperType>
using GetApiT = typename GetValue<WrapperType, ApiWrappers>::Type;

}//namespace Fred::Identity::Impl::Wrapper::{anonymous}

}//namespace Fred::Identity::Impl::Wrapper
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#include "src/impl/api_type_setter_impl.hh" // must be included after GetApiT defining

namespace Fred {
namespace Identity {
namespace Impl {

template <>
template <>
ApiRepeatedTypeSetter<Wrapper::ContactId>::ApiRepeatedTypeSetter(::google::protobuf::RepeatedPtrField<Api::ContactId>* ptr) noexcept
    : ApiData{ptr}
{ }

template class ApiRepeatedTypeSetter<Wrapper::ContactId>;

namespace Wrapper {

template <>
GetContactsByIdentityReply::Data::Data(Api::GetContactsByIdentityReply::Data* ptr) noexcept
    : ApiData{ptr}
{ }

ApiRepeatedTypeSetter<ContactId> GetContactsByIdentityReply::Data::set_contact_ids() const
{
    return ApiRepeatedTypeSetter<ContactId>{this->ApiData::get<Api::GetContactsByIdentityReply::Data>()->mutable_contact_ids()};
}

template <>
GetContactsByIdentityReply::GetContactsByIdentityReply(Api::GetContactsByIdentityReply* ptr) noexcept
    : ApiData{ptr}
{ }

GetContactsByIdentityReply::Data GetContactsByIdentityReply::set_data() const
{
    return Data{this->ApiData::get<Api::GetContactsByIdentityReply>()->mutable_data()};
}

}//namespace Fred::Identity::Impl::Wrapper
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred
