/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/wrapper/common_types.hh"
#include "src/impl/get_value.hh"

#include "fred_api/identity/common_types.pb.h"

#include <boost/uuid/uuid_io.hpp>

#include <type_traits>
#include <utility>

namespace Fred {
namespace Identity {
namespace Impl {
namespace Wrapper {

namespace {

using ApiWrappers = std::tuple<KeyValuePair<Uuid, Api::Uuid>,
                               KeyValuePair<ContactId, Api::ContactId>,
                               KeyValuePair<ContactHistoryId, Api::ContactHistoryId>,
                               KeyValuePair<Date, Api::Date>,
                               KeyValuePair<PlaceAddress, Api::PlaceAddress>>;

template <typename WrapperType>
using GetApiT = typename GetValue<WrapperType, ApiWrappers>::Type;

}//namespace Fred::Identity::Impl::Wrapper::{anonymous}

}//namespace Fred::Identity::Impl::Wrapper
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#include "src/impl/api_type_setter_impl.hh" // must be included after GetApiT defining

namespace Fred {
namespace Identity {
namespace Impl {
namespace Wrapper {

template <>
Uuid::Uuid(Api::Uuid* ptr) noexcept
    : ApiData{ptr}
{ }

const Uuid& Uuid::set_value(const boost::uuids::uuid& value) const
{
    this->ApiData::get<Api::Uuid>()->set_value(to_string(value));
    return *this;
}

template <>
ContactId::ContactId(Api::ContactId* ptr) noexcept
    : ApiData{ptr}
{ }

Uuid ContactId::set_uuid() const
{
    return Uuid{this->ApiData::get<Api::ContactId>()->mutable_uuid()};
}

const ContactId& ContactId::set_numeric_id(std::uint64_t numeric_id) const
{
    this->ApiData::get<Api::ContactId>()->set_numeric_id(numeric_id);
    return *this;
}

const ContactId& ContactId::set_handle(std::string handle) const
{
    this->ApiData::get<Api::ContactId>()->set_handle(std::move(handle));
    return *this;
}

template <>
ContactHistoryId::ContactHistoryId(Api::ContactHistoryId* ptr) noexcept
    : ApiData{ptr}
{ }

Uuid ContactHistoryId::set_uuid() const
{
    return Uuid{this->ApiData::get<Api::ContactHistoryId>()->mutable_uuid()};
}

template <>
Date::Date(Api::Date* ptr) noexcept
    : ApiData{ptr}
{ }

void Date::set_year(int value) const
{
    this->ApiData::get<Api::Date>()->set_year(value);
}

void Date::set_month(int value) const
{
    this->ApiData::get<Api::Date>()->set_month(value);
}

void Date::set_day(int value) const
{
    this->ApiData::get<Api::Date>()->set_day(value);
}

template <>
PlaceAddress::PlaceAddress(Api::PlaceAddress* ptr) noexcept
    : ApiData{ptr}
{ }

const PlaceAddress& PlaceAddress::set_street(const std::string* begin, const std::string* end) const
{
    auto* const street = this->ApiData::get<Api::PlaceAddress>()->mutable_street();
    street->Reserve(end - begin);
    std::for_each(begin, end, [&](const std::string& street_part) { *(street->Add()) = street_part; });
    return *this;
}

const PlaceAddress& PlaceAddress::set_city(const std::string& value) const
{
    *(this->ApiData::get<Api::PlaceAddress>()->mutable_city()) = value;
    return *this;
}

const PlaceAddress& PlaceAddress::set_state_or_province(const std::string& value) const
{
    *(this->ApiData::get<Api::PlaceAddress>()->mutable_state_or_province()) = value;
    return *this;
}

const PlaceAddress& PlaceAddress::set_postal_code(const std::string& value) const
{
    *(this->ApiData::get<Api::PlaceAddress>()->mutable_postal_code()) = value;
    return *this;
}

const PlaceAddress& PlaceAddress::set_country_code(const std::string& value) const
{
    *(this->ApiData::get<Api::PlaceAddress>()->mutable_country_code()) = value;
    return *this;
}

}//namespace Fred::Identity::Impl::Wrapper
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred
