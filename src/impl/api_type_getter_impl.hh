/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef API_TYPE_GETTER_IMPL_HH_26FDA0DE0EAC0FF85254ED5E2D853BAB//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define API_TYPE_GETTER_IMPL_HH_26FDA0DE0EAC0FF85254ED5E2D853BAB

#include "src/impl/api_type_getter.hh"

namespace Fred {
namespace Identity {
namespace Impl {

template <typename UnwrapperType>
template <typename ApiType>
ApiTypeGetter<UnwrapperType>::ApiTypeGetter(const ApiType& ref) noexcept
    : api_data_{reinterpret_cast<const void*>(&ref)}
{
    static_assert(std::is_same<Unwrapper::GetApiT<UnwrapperType>, ApiType>::value, "ApiType does not correspond to UnwrapperType");
}

template <typename UnwrapperType>
template <typename ApiType>
const ApiType& ApiTypeGetter<UnwrapperType>::get() const noexcept
{
    static_assert(std::is_same<Unwrapper::GetApiT<UnwrapperType>, ApiType>::value, "ApiType does not correspond to UnwrapperType");
    return *reinterpret_cast<const ApiType*>(api_data_);
}

}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#endif//API_TYPE_GETTER_HH_26FDA0DE0EAC0FF85254ED5E2D853BAB
