/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef API_TYPE_SETTER_HH_7E235603CDD9FD862BE82A6D2A97B7FB//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define API_TYPE_SETTER_HH_7E235603CDD9FD862BE82A6D2A97B7FB

namespace Fred {
namespace Identity {
namespace Impl {

template <typename Wrapper>
class ApiTypeSetter
{
protected:
    using ApiData = ApiTypeSetter;
    template <typename ApiType>
    explicit ApiTypeSetter(ApiType* ptr) noexcept;
    ApiTypeSetter(ApiTypeSetter&&) noexcept = default;
    ApiTypeSetter& operator=(ApiTypeSetter&&) noexcept = default;
    template <typename ApiType>
    ApiType* get() const noexcept;
public:
    ApiTypeSetter() = delete;
    ApiTypeSetter(const ApiTypeSetter&) = delete;
    ApiTypeSetter& operator=(const ApiTypeSetter&) = delete;
private:
    void* api_data_;
};

template <typename T>
class ApiRepeatedTypeSetter : private ApiTypeSetter<ApiRepeatedTypeSetter<T>>
{
public:
    template <typename ApiType>
    explicit ApiRepeatedTypeSetter(ApiType* ptr) noexcept;
    const ApiRepeatedTypeSetter& reserve(int new_size) const;
    T add() const;
    int capacity() const;
    bool empty() const;
    int size() const;
private:
    using ApiData = typename ApiTypeSetter<ApiRepeatedTypeSetter<T>>::ApiData;
};

}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#endif//API_TYPE_SETTER_HH_7E235603CDD9FD862BE82A6D2A97B7FB
