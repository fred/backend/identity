/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef STRONG_TYPE_HH_DBC32F1D098B7C9C18E4B86E3356345E//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define STRONG_TYPE_HH_DBC32F1D098B7C9C18E4B86E3356345E

#include "libstrong/type.hh"

#include <spdlog/fmt/fmt.h>

#include <utility>


namespace Fred {
namespace Identity {
namespace Impl {

template <typename Type, typename Tag, template <typename> class ...Skills>
using StrongType = LibStrong::TypeWithSkills<Type, Tag, Skills...>;

template <typename T>
using Optional = LibStrong::Optional<T>;

namespace Skill = LibStrong::Skill;

}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

// Enable LibStrong::Type in LibLog
template <typename Type, typename Tag, template <typename> class Skill>
struct fmt::formatter<LibStrong::Type<Type, Tag, Skill>, char> : fmt::formatter<Type>
{
    template <typename FormatCtx>
    decltype(auto) format(const LibStrong::Type<Type, Tag, Skill>& value, FormatCtx&& ctx)
    {
        return fmt::formatter<Type>::format(*value, std::forward<FormatCtx>(ctx));
    }
};

#endif//STRONG_TYPE_HH_DBC32F1D098B7C9C18E4B86E3356345E
