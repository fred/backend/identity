/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/lib/contact_state_flags.hh"


namespace Fred {
namespace Identity {
namespace Impl {
namespace Lib {

std::string wrap_into_psql_representation(ContactNameChangeProhibitedFlag)
{
    return flag_name<ContactStateFlag::ServerContactNameChangeProhibited>();
}

std::string wrap_into_psql_representation(ContactOrganizationChangeProhibitedFlag)
{
    return flag_name<ContactStateFlag::ServerContactOrganizationChangeProhibited>();
}

std::string wrap_into_psql_representation(ContactIdentChangeProhibitedFlag)
{
    return flag_name<ContactStateFlag::ServerContactIdentChangeProhibited>();
}

std::string wrap_into_psql_representation(ContactPermanentAddressChangeProhibitedFlag)
{
    return flag_name<ContactStateFlag::ServerContactPermanentAddressChangeProhibited>();
}

template <>
std::string flag_name<ContactStateFlag::ServerContactNameChangeProhibited>()
{
    return "serverContactNameChangeProhibited";
}

template <>
std::string flag_name<ContactStateFlag::ServerContactOrganizationChangeProhibited>()
{
    return "serverContactOrganizationChangeProhibited";
}

template <>
std::string flag_name<ContactStateFlag::ServerContactIdentChangeProhibited>()
{
    return "serverContactIdentChangeProhibited";
}

template <>
std::string flag_name<ContactStateFlag::ServerContactPermanentAddressChangeProhibited>()
{
    return "serverContactPermanentAddressChangeProhibited";
}

}//namespace Fred::Identity::Impl::Lib
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred
