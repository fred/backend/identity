/*
 * Copyright (C) 2021-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/lib/attach_identity.hh"
#include "src/impl/lib/create_contact_state_request.hh"
#include "src/impl/lib/unwrap_uuid_from_psql.hh"
#include "src/impl/lib/wrap_uuid_into_psql.hh"
#include "src/impl/exceptions.hh"
#include "src/impl/strong_type.hh"
#include "src/util/util.hh"

#include "liblog/liblog.hh"

#include "libpg/query.hh"
#include "libpg/sql_state/code.hh"

#include <boost/optional.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <spdlog/fmt/fmt.h>

#include <sstream>
#include <string>
#include <tuple>
#include <utility>


template <>
struct fmt::formatter<LibPg::PgConnection::ExecResult::ErrorCode, char> : fmt::formatter<std::string>
{
    template <typename FormatCtx>
    decltype(auto) format(const LibPg::PgConnection::ExecResult::ErrorCode& code, FormatCtx&& ctx)
    {
        std::ostringstream out;
        out << code;
        return fmt::formatter<std::string>::format(out.str(), std::forward<FormatCtx>(ctx));
    }
};

namespace Fred {
namespace Identity {
namespace Impl {
namespace Lib {

namespace  {

using ContactHistoryUuid = StrongType<boost::uuids::uuid, struct ContactHistoryUuidTag_>;
using IdentityProvider = StrongType<std::string, struct IdentityProviderTag_>;
using Subject = StrongType<std::string, struct SubjectTag_>;
using ContactUuid = StrongType<boost::uuids::uuid, struct ContactUuidTag_>;
using IdentityId = StrongType<std::uint64_t, struct IdentityIdTag_>;
using ContactHandle = StrongType<std::string, struct ContactHandleTag_>;
using ContactOrganizationEmpty = StrongType<bool, struct ContactOrganizationEmptyTag_>;

bool is_mojeid_contact(
        const LibPg::PgRwTransaction& tx,
        const ContactUuid& contact_uuid)
{
    const auto dbres = exec(
            tx,
            LibPg::make_query() <<
                    "SELECT" << LibPg::item<bool>() <<
                               "EXISTS(SELECT 0 "
                                      "FROM object_state os "
                                      "JOIN enum_object_states eos ON eos.id = os.state_id "
                                      "WHERE os.object_id = object_registry.id AND "
                                            "os.valid_to IS NULL AND "
                                            "eos.name = 'mojeidContact') "
                    "FROM object_registry "
                    "WHERE uuid = " << LibPg::parameter<ContactUuid>().as_type("UUID") << " AND "
                          "erdate IS NULL AND "
                          "type = get_object_type_id('contact'::TEXT) "
                    "FOR UPDATE",
            contact_uuid);
    if (dbres.empty())
    {
        struct ContactNotFound : ContactDoesNotExist
        {
            const char* what() const noexcept override { return "no contact found by uuid"; }
        };
        throw ContactNotFound{};
    }
    if (LibPg::RowIndex{1} < dbres.size())
    {
        struct UnexpectedDatabaseResult : Exception
        {
            const char* what() const noexcept override { return "too many contacts was found"; }
        };
        throw UnexpectedDatabaseResult{};
    }
    return dbres.front().get<bool>();
}

decltype(auto) attach_identity(
        const LibPg::PgRwTransaction& tx,
        const ContactUuid& contact_uuid,
        const IdentityProvider& provider,
        const Subject& subject)
{
    const auto dbres = exec(
            tx,
            LibPg::make_query(
                    "INSERT INTO contact_identity "
                        "(contact_id, identity_provider, subject) "
                        "SELECT id,") <<
                                LibPg::parameter<IdentityProvider>().as_varchar() << ", " <<
                                LibPg::parameter<Subject>().as_varchar() << " "
                        "FROM object_registry "
                        "WHERE uuid = " << LibPg::parameter<ContactUuid>().as_type("UUID") << " AND "
                              "erdate IS NULL AND "
                              "type = get_object_type_id('contact') "
                    "RETURNING" << LibPg::item<IdentityId>() << "id"
                                << LibPg::item<ContactId>() << "contact_id"
                                << LibPg::item<ContactHandle>() << "(SELECT name FROM object_registry WHERE id = contact_identity.contact_id)"
                                << LibPg::nullable_item<ContactOrganizationEmpty>() <<
                                "(SELECT TRIM(organization) = '' "
                                   "FROM contact "
                                  "WHERE id = contact_identity.contact_id)",
            provider,
            subject,
            contact_uuid);
    if (dbres.empty())
    {
        struct ContactNotFound : ContactDoesNotExist
        {
            const char* what() const noexcept override { return "no contact found by uuid"; }
        };
        throw ContactNotFound{};
    }
    if (LibPg::RowIndex{1} < dbres.size())
    {
        struct UnexpectedDatabaseResult : Exception
        {
            const char* what() const noexcept override { return "too many contacts was found"; }
        };
        throw UnexpectedDatabaseResult{};
    }
    if (!dbres.front().is_null<ContactOrganizationEmpty>() && !*dbres.front().get_nullable<ContactOrganizationEmpty>())
    {
        throw ContactIsOrganization{};
    }
    return std::make_tuple(dbres.front().get<IdentityId>(),
                           dbres.front().get<ContactId>(),
                           dbres.front().get<ContactHandle>());
}

decltype(auto) attach_identity(
        const LibPg::PgRwTransaction& tx,
        const ContactUuid& contact_uuid,
        const ContactHistoryUuid& contact_history_uuid,
        const IdentityProvider& provider,
        const Subject& subject)
{
    const auto dbres = exec(
            tx,
            LibPg::make_query(
                    "INSERT INTO contact_identity "
                        "(contact_id, identity_provider, subject) "
                        "SELECT obr.id, ") <<
                                LibPg::parameter<IdentityProvider>().as_varchar() << ", " <<
                                LibPg::parameter<Subject>().as_varchar() << " "
                        "FROM object_registry obr "
                        "JOIN history h ON h.id = obr.historyid "
                        "WHERE obr.uuid = " << LibPg::parameter<ContactUuid>().as_type("UUID") << " AND "
                              "obr.erdate IS NULL AND "
                              "obr.type = get_object_type_id('contact') AND "
                              "h.uuid = " << LibPg::parameter<ContactHistoryUuid>().as_type("UUID") << " "
                    "RETURNING" << LibPg::item<IdentityId>() << "id"
                                << LibPg::item<ContactId>() << "contact_id"
                                << LibPg::item<ContactHandle>() << "(SELECT name FROM object_registry WHERE id = contact_identity.contact_id)"
                                << LibPg::nullable_item<ContactOrganizationEmpty>() <<
                                "(SELECT TRIM(organization) = '' "
                                   "FROM contact "
                                  "WHERE id = contact_identity.contact_id)",
            provider,
            subject,
            contact_uuid,
            contact_history_uuid);
    if (dbres.empty())
    {
        struct ContactNotFound : ContactDoesNotExist
        {
            const char* what() const noexcept override { return "no contact by uuid and history_uuid found"; }
        };
        throw ContactNotFound{};
    }
    if (LibPg::RowIndex{1} < dbres.size())
    {
        struct UnexpectedDatabaseResult : Exception
        {
            const char* what() const noexcept override { return "too many contacts was found"; }
        };
        throw UnexpectedDatabaseResult{};
    }
    if (!dbres.front().is_null<ContactOrganizationEmpty>() && !*dbres.front().get_nullable<ContactOrganizationEmpty>())
    {
        throw ContactIsOrganization{};
    }
    return std::make_tuple(dbres.front().get<IdentityId>(),
                           dbres.front().get<ContactId>(),
                           dbres.front().get<ContactHandle>());
}

template <typename Flag>
void set(ContactState& state, bool value)
{
    std::get<Flag>(state) = Flag{value};
}

}//namespace Fred::Identity::Impl::Lib::{anonymous}

void attach_identity(
        const LibPg::PgRwTransaction& tx,
        const Unwrapper::AttachIdentityRequest& request)
{
    try
    {
        if (is_mojeid_contact(tx, ContactUuid{request.get_contact_id().get_uuid().get_value()}))
        {
            struct IsMojeidContact : IdentityAttached
            {
                const char* what() const noexcept override { return "contact already attached to mojeID"; }
            };
            throw IsMojeidContact{};
        }
        const auto inserted = [&]()
        {
            if (request.has_contact_history_id())
            {
                return attach_identity(
                        tx,
                        ContactUuid{request.get_contact_id().get_uuid().get_value()},
                        ContactHistoryUuid{request.get_contact_history_id().get_uuid().get_value()},
                        IdentityProvider{request.get_identity().get_identity_provider().get_value()},
                        Subject{request.get_identity().get_subject().get_value()});
            }
            return attach_identity(
                    tx,
                    ContactUuid{request.get_contact_id().get_uuid().get_value()},
                    IdentityProvider{request.get_identity().get_identity_provider().get_value()},
                    Subject{request.get_identity().get_subject().get_value()});
        }();
        const auto contact_state = [&]()
        {
            ContactState state;
            const bool to_protect_name = request.has_identity_data() && request.get_identity_data().has_name();
            const bool to_protect_address = request.has_identity_data() && request.get_identity_data().has_address();
            const bool to_protect_birthdate = request.has_identity_data() && request.get_identity_data().has_birthdate();
            static constexpr bool to_protect_organization = true;
            set<ContactStateFlag::ServerContactNameChangeProhibited>(state, to_protect_name);
            set<ContactStateFlag::ServerContactPermanentAddressChangeProhibited>(state, to_protect_address);
            set<ContactStateFlag::ServerContactIdentChangeProhibited>(state, to_protect_birthdate);
            set<ContactStateFlag::ServerContactOrganizationChangeProhibited>(state, to_protect_organization);
            return state;
        }();
        create_contact_state_request(tx, std::get<ContactId>(inserted), contact_state);
        LIBLOG_DEBUG("Identity {} added to the contact ({}, '{}')", std::get<IdentityId>(inserted),
                                                                    std::get<ContactId>(inserted),
                                                                    std::get<ContactHandle>(inserted));
    }
    catch (const ContactDoesNotExist& e)
    {
        LIBLOG_INFO("contact not found: {}", e.what());
        throw;
    }
    catch (const IdentityAttached& e)
    {
        LIBLOG_INFO("identity already exists: {}", e.what());
        throw;
    }
    catch (const LibPg::ExecFailure& e)
    {
        if (e.error_code() == LibPg::SqlState::unique_violation)
        {
            LIBLOG_INFO("identity already exists: {}", e.what());
            struct ConstraintViolation : IdentityAttached
            {
                const char* what() const noexcept override { return "identity already exists"; }
            };
            throw ConstraintViolation{};
        }
        LIBLOG_ERROR("exception caught: {} {}", e.error_code(), e.what());
        throw;
    }
    catch (const std::exception& e)
    {
        LIBLOG_ERROR("exception caught: {}", e.what());
        throw;
    }
    catch (...)
    {
        LIBLOG_ERROR("unexpected exception caught");
        throw;
    }
}

}//namespace Fred::Identity::Impl::Lib
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred
