/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CHECK_CURRENT_CONTACT_DATA_HH_EE355280570D3863F0A01669AF218103//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define CHECK_CURRENT_CONTACT_DATA_HH_EE355280570D3863F0A01669AF218103

#include "src/impl/unwrapper/check_current_contact_data_request.hh"
#include "src/impl/wrapper/common_types.hh"

#include "libpg/pg_rw_transaction.hh"

namespace Fred {
namespace Identity {
namespace Impl {
namespace Lib {

template <typename Reply>
void check_current_contact_data(const LibPg::PgRwTransaction& tx, const Unwrapper::CheckCurrentContactDataRequest& request, Reply* response);

}//namespace Fred::Identity::Impl::Lib
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#endif//CHECK_CURRENT_CONTACT_DATA_HH_EE355280570D3863F0A01669AF218103
