/*
 * Copyright (C) 2021-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/lib/check_current_contact_data.hh"

#include "src/impl/exceptions.hh"
#include "src/impl/lib/unwrap_uuid_from_psql.hh"
#include "src/impl/lib/wrap_uuid_into_psql.hh"
#include "src/impl/strong_type.hh"
#include "src/impl/unwrapper/check_current_contact_data_request.hh"
#include "src/impl/wrapper/check_current_contact_data_reply.hh"

#include "src/util/util.hh"

#include "fred_api/identity/service_identity_checker_grpc.pb.h"

#include "libfred/object/check_authinfo.hh"

#include "liblog/liblog.hh"

#include "libpg/pg_ro_transaction.hh"
#include "libpg/query.hh"

#include <boost/uuid/uuid_io.hpp>

#include <array>
#include <tuple>
#include <utility>


namespace Fred {
namespace Identity {
namespace Impl {
namespace Lib {

namespace  {

using ContactUuid = StrongType<boost::uuids::uuid, struct ContactUuidTag_>;
using ContactHistoryUuid = StrongType<boost::uuids::uuid, struct ContactHistoryUuidTag_>;
using AuthInfo = StrongType<std::string, struct AuthInfoTag_>;
using AuthInfoMatch = StrongType<bool, struct AuthInfoMatchTag_>;
using Name = StrongType<std::string, struct NameTag_>;
using NameMatch = StrongType<bool, struct NameMatchTag_>;
using OrganizationMatch = StrongType<bool, struct OrganizationMatchTag_>;
using AddressMatch = StrongType<bool, struct AddressMatchTag_>;
using Street1 = StrongType<std::string, struct Street1Tag_>;
using Street2 = StrongType<std::string, struct Street2Tag_>;
using Street3 = StrongType<std::string, struct Street3Tag_>;
using City = StrongType<std::string, struct CityTag_>;
using StateOrProvince = StrongType<std::string, struct StateOrProvinceag_>;
using PostalCode = StrongType<std::string, struct PostalCodeTag_>;
using CountryCode = StrongType<std::string, struct CountryCodeTag_>;
using Birthdate = StrongType<std::string, struct BirthdateTag_>;
using BirthdateMatch = StrongType<bool, struct BirthdateMatchTag_>;
struct Date
{
    int day;
    int month;
    int year;
    friend bool operator==(const Date& lhs, const Date& rhs)
    {
        return (lhs.year == rhs.year) &&
               (lhs.month == rhs.month) &&
               (lhs.day == rhs.day);
    }
};
static constexpr Date no_date = {0, 0, 0};
using ContactId = StrongType<std::uint64_t, struct ContactIdTag_>;
using ContactHandle = StrongType<std::string, struct ContactHandleTag_>;

decltype(auto) compare_current_contact_data(
        const LibPg::PgRwTransaction& tx,
        const ContactUuid& contact_id,
        const Optional<ContactHistoryUuid>& contact_history_id,
        const AuthInfo& auth_info,
        const Name& name,
        const Street1& street1,
        const Street2& street2,
        const Street3& street3,
        const City& city,
        const StateOrProvince& state_or_province,
        const PostalCode& postal_code,
        const CountryCode& country_code,
        const Date& birthdate)
{
    const auto dbres = exec(
            tx,
            LibPg::make_query("SELECT") << LibPg::nullable_item<ContactHistoryUuid>() << "h.uuid"
                                        << LibPg::item<ContactId>() << "obr.id"
                                        << LibPg::item<ContactHandle>() << "UPPER(obr.name)"
                                        << LibPg::item<NameMatch>() << "COALESCE(LOWER(REGEXP_REPLACE(TRIM(c.name),'\\s\\s+',' ','g')),'')="
                                                                       "LOWER(REGEXP_REPLACE(TRIM(" << LibPg::parameter<Name>().as_text() << "),'\\s\\s+',' ','g'))"
                                        << LibPg::item<OrganizationMatch>() << "COALESCE(TRIM(c.organization) = '', TRUE)"
                                        << LibPg::item<AddressMatch>() << "COALESCE(LOWER(REGEXP_REPLACE(TRIM(c.street1),'\\s\\s+',' ','g')),'')="
                                                                          "LOWER(REGEXP_REPLACE(TRIM(" << LibPg::parameter<Street1>().as_text() << "),'\\s\\s+',' ','g')) AND "
                                                                          "COALESCE(LOWER(REGEXP_REPLACE(TRIM(c.street2),'\\s\\s+',' ','g')),'')="
                                                                          "LOWER(REGEXP_REPLACE(TRIM(" << LibPg::parameter<Street2>().as_text() << "),'\\s\\s+',' ','g')) AND "
                                                                          "COALESCE(LOWER(REGEXP_REPLACE(TRIM(c.street3),'\\s\\s+',' ','g')),'')="
                                                                          "LOWER(REGEXP_REPLACE(TRIM(" << LibPg::parameter<Street3>().as_text() << "),'\\s\\s+',' ','g')) AND "
                                                                          "COALESCE(LOWER(REGEXP_REPLACE(TRIM(c.city),'\\s\\s+',' ','g')),'')="
                                                                          "LOWER(REGEXP_REPLACE(TRIM(" << LibPg::parameter<City>().as_text() << "),'\\s\\s+',' ','g')) AND "
                                                                          "COALESCE(LOWER(REGEXP_REPLACE(TRIM(c.stateorprovince),'\\s\\s+',' ','g')),'')="
                                                                          "LOWER(REGEXP_REPLACE(TRIM(" << LibPg::parameter<StateOrProvince>().as_text() << "),'\\s\\s+',' ','g')) AND "
                                                                          "COALESCE(LOWER(REGEXP_REPLACE(TRIM(c.postalcode),'\\s\\s+',' ','g')),'')="
                                                                          "LOWER(REGEXP_REPLACE(TRIM(" << LibPg::parameter<PostalCode>().as_text() << "),'\\s\\s+',' ','g')) AND "
                                                                          "COALESCE(UPPER(REGEXP_REPLACE(TRIM(c.country),'\\s\\s+',' ','g')),'')="
                                                                          "UPPER(REGEXP_REPLACE(TRIM(" << LibPg::parameter<CountryCode>().as_text() << "),'\\s\\s+',' ','g'))"
                                        << LibPg::item<Name>() << "COALESCE(REGEXP_REPLACE(TRIM(c.name),'\\s\\s+',' ','g'),'')"
                                        << LibPg::item<Street1>() << "COALESCE(REGEXP_REPLACE(TRIM(c.street1),'\\s\\s+',' ','g'),'')"
                                        << LibPg::item<Street2>() << "COALESCE(REGEXP_REPLACE(TRIM(c.street2),'\\s\\s+',' ','g'),'')"
                                        << LibPg::item<Street3>() << "COALESCE(REGEXP_REPLACE(TRIM(c.street3),'\\s\\s+',' ','g'),'')"
                                        << LibPg::item<City>() << "COALESCE(REGEXP_REPLACE(TRIM(c.city),'\\s\\s+',' ','g'),'')"
                                        << LibPg::item<StateOrProvince>() << "COALESCE(REGEXP_REPLACE(TRIM(c.stateorprovince),'\\s\\s+',' ','g'),'')"
                                        << LibPg::item<PostalCode>() << "COALESCE(REGEXP_REPLACE(TRIM(c.postalcode),'\\s\\s+',' ','g'),'')"
                                        << LibPg::item<CountryCode>() << "COALESCE(REGEXP_REPLACE(TRIM(c.country),'\\s\\s+',' ','g'),'')"
                                        << LibPg::nullable_item<Birthdate>() << "(SELECT TRIM(c.ssn) FROM enum_ssntype e "
                                                                                 "WHERE e.id=c.ssntype AND e.type='BIRTHDAY' AND TRIM(c.ssn)!='') "
                              "FROM object_registry obr "
                              "LEFT JOIN history h ON h.id=obr.historyid AND "
                                                     "COALESCE(h.uuid=" << LibPg::parameter<Optional<ContactHistoryUuid>>().as_type("UUID") << ",TRUE) "
                              "JOIN contact c ON c.id=obr.id "
                              "JOIN object o ON o.id=obr.id "
                              "WHERE obr.type=get_object_type_id('contact') AND "
                                    "obr.erdate IS NULL AND "
                                    "obr.uuid=" << LibPg::parameter<ContactUuid>().as_type("UUID"),
            contact_id,
            contact_history_id,
            name,
            street1,
            street2,
            street3,
            city,
            state_or_province,
            postal_code,
            country_code);
    if (dbres.empty())
    {
        struct ContactNotFound : ContactDoesNotExist
        {
            const char* what() const noexcept override { return "no existing contact has given uuid"; }
        };
        throw ContactNotFound{};
    }
    if (LibPg::RowIndex{1} < dbres.size())
    {
        struct UnexpectedDatabaseResult : Exception
        {
            const char* what() const noexcept override { return "too many contacts was found"; }
        };
        throw UnexpectedDatabaseResult{};
    }
    if (dbres.front().is_null<ContactHistoryUuid>())
    {
        struct ContactHistoryIdMismatch : HistoryIdMismatch
        {
            const char* what() const noexcept override { return "current contact history differs from a given history uuid"; }
        };
        throw ContactHistoryIdMismatch{};
    }
    const bool contact_has_birthdate = !dbres.front().is_null<Birthdate>();
    Date contact_birthdate = no_date;
    auto birthdate_data = Birthdate{""};
    if (contact_has_birthdate)
    {
        birthdate_data = dbres.front().get_nullable<Birthdate>();
        try
        {
            const auto date = Util::birthdate_from_string_to_date(*birthdate_data);
            contact_birthdate.year = date.year();
            contact_birthdate.month = date.month();
            contact_birthdate.day = date.day();
        }
        catch (...)
        {
            contact_birthdate = no_date;
        }
    }
    const auto contact_numeric_id = dbres.front().get<ContactId>();
    const bool authinfo_match = 0 < LibFred::Object::CheckAuthinfo{LibFred::Object::ObjectId{*contact_numeric_id}}
            .exec(tx, *auth_info, LibFred::Object::CheckAuthinfo::increment_usage_and_cancel);
    return std::make_tuple(dbres.front().get_nullable<ContactHistoryUuid>(),
                           contact_id,
                           dbres.front().get<ContactHandle>(),
                           AuthInfoMatch{authinfo_match},
                           dbres.front().get<NameMatch>(),
                           dbres.front().get<OrganizationMatch>(),
                           dbres.front().get<AddressMatch>(),
                           BirthdateMatch{contact_has_birthdate && (contact_birthdate == birthdate)},
                           dbres.front().get<Name>(),
                           dbres.front().get<Street1>(),
                           dbres.front().get<Street2>(),
                           dbres.front().get<Street3>(),
                           dbres.front().get<City>(),
                           dbres.front().get<StateOrProvince>(),
                           dbres.front().get<PostalCode>(),
                           dbres.front().get<CountryCode>(),
                           Birthdate{contact_has_birthdate ? *birthdate_data : std::string{}},
                           contact_birthdate);
}

template <int street>
decltype(auto) extract_street(const Unwrapper::PlaceAddress& address)
{
    static_assert(1 <= street);
    static_assert(street <= 3);
    static constexpr auto index = street - 1;
    return index < address.get_street().size() ? address.get_street()[index] : std::string{};
}

}//namespace Fred::Identity::Impl::Lib::{anonymous}

template <>
void check_current_contact_data(const LibPg::PgRwTransaction& tx, const Unwrapper::CheckCurrentContactDataRequest& request, Api::CheckCurrentContactDataReply* response)
{
    try
    {
        if (!request.has_contact_id())
        {
            struct NoContactId : MissingContactId
            {
                const char* what() const noexcept override { return "missing contact_id parameter"; }
            };
            throw NoContactId{};
        }
        const auto address = request.get_address();
        const auto result = compare_current_contact_data(
                tx,
                ContactUuid{request.get_contact_id().get_uuid().get_value()},
                request.has_contact_history_id() ? Optional<ContactHistoryUuid>{ContactHistoryUuid{request.get_contact_history_id().get_uuid().get_value()}}
                                                 : ContactHistoryUuid::nullopt,
                AuthInfo{request.get_auth_info()},
                Name{request.get_name()},
                Street1{extract_street<1>(address)},
                Street2{extract_street<2>(address)},
                Street3{extract_street<3>(address)},
                City{address.get_city()},
                StateOrProvince{address.get_state_or_province()},
                PostalCode{address.get_postal_code()},
                CountryCode{address.get_country_code()},
                Date{request.get_birthdate().get_day(),
                     request.get_birthdate().get_month(),
                     request.get_birthdate().get_year()});
        response->Clear();
        if (!*std::get<OrganizationMatch>(result))
        {
            throw ContactIsOrganization{};
        }
        auto data = Wrapper::CheckCurrentContactDataReply{response}.set_data();
        data.set_contact_history_id().set_uuid().set_value(*std::get<ContactHistoryUuid>(result));
        data.set_auth_info_matches(*std::get<AuthInfoMatch>(result));
        if (!*std::get<NameMatch>(result))
        {
            data.set_name(*std::get<Name>(result));
        }
        if (!*std::get<AddressMatch>(result))
        {
            const auto street_parts = std::array<std::string, 3>{*std::get<Street1>(result), *std::get<Street2>(result), *std::get<Street3>(result)};
            if (!street_parts[2].empty())
            {
                data.set_address().set_street(street_parts.data(), street_parts.data() + 3);
            }
            else if (!street_parts[1].empty())
            {
                data.set_address().set_street(street_parts.data(), street_parts.data() + 2);
            }
            else if (!street_parts[0].empty())
            {
                data.set_address().set_street(street_parts.data(), street_parts.data() + 1);
            }
            data.set_address().set_city(*std::get<City>(result));
            data.set_address().set_state_or_province(*std::get<StateOrProvince>(result));
            data.set_address().set_postal_code(*std::get<PostalCode>(result));
            data.set_address().set_country_code(*std::get<CountryCode>(result));
        }
        if (!*std::get<BirthdateMatch>(result))
        {
            const auto birthdate = std::get<Date>(result);
            if (birthdate == no_date)
            {
                data.set_broken_birthdate(*std::get<Birthdate>(result));
            }
            else
            {
                const auto birthdate_setter = data.set_birthdate();
                birthdate_setter.set_year(birthdate.year);
                birthdate_setter.set_month(birthdate.month);
                birthdate_setter.set_day(birthdate.day);
            }
        }
    }
    catch (const InvalidArgument& e)
    {
        LIBLOG_INFO("InvalidArgument: {}", e.what());
        throw;
    }
    catch (const ContactDoesNotExist& e)
    {
        LIBLOG_INFO("Contact not found: {}", e.what());
        throw;
    }
    catch (const HistoryIdMismatch& e)
    {
        LIBLOG_INFO("HistoryIdMismatch: {}", e.what());
        throw;
    }
    catch (const std::exception& e)
    {
        LIBLOG_ERROR("exception caught: {}", e.what());
        throw;
    }
    catch (...)
    {
        LIBLOG_ERROR("unexpected exception caught");
        throw;
    }
}

}//namespace Fred::Identity::Impl::Lib
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred
