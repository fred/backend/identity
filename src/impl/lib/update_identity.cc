/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/lib/update_identity.hh"
#include "src/cfg.hh"
#include "src/impl/lib/create_contact_state_request.hh"
#include "src/impl/lib/unwrap_uuid_from_psql.hh"
#include "src/impl/lib/wrap_uuid_into_psql.hh"
#include "src/impl/exceptions.hh"
#include "src/impl/strong_type.hh"
#include "src/util/util.hh"

#include "libfred/registrable_object/contact/update_contact.hh"
#include "libfred/poll/create_update_object_poll_message.hh"

#include "liblog/liblog.hh"

#include "libpg/query.hh"
#include "libpg/sql_state/code.hh"

#include <boost/optional.hpp>

#include <iomanip>
#include <sstream>
#include <string>
#include <tuple>
#include <utility>


namespace Fred {
namespace Identity {
namespace Impl {
namespace Lib {

namespace  {

using IdentityProvider = StrongType<std::string, struct IdentityProviderTag_>;
using Subject = StrongType<std::string, struct SubjectTag_>;
using ContactHistoryId = StrongType<std::uint64_t, struct ContactHistoryIdTag_>;
using IdentityId = StrongType<std::uint64_t, struct IdentityIdTag_>;
using ContactHandle = StrongType<std::string, struct ContactHandleTag_>;
using NameChangeProhibited = StrongType<bool, struct NameChangeProhibitedTag_>;
using OrganizationChangeProhibited = StrongType<bool, struct OrganizationChangeProhibitedTag_>;
using IdentChangeProhibited = StrongType<bool, struct IdentChangeProhibitedTag_>;
using PermanentAddressChangeProhibited = StrongType<bool, struct PermanentAddressChangeProhibitedTag_>;
using NameChangeRequested = StrongType<bool, struct NameChangeRequestedTag_>;
using IdentChangeRequested = StrongType<bool, struct IdentChangeRequestedTag_>;
using PermanentAddressChangeRequested = StrongType<bool, struct PermanentAddressChangeRequestedTag_>;
using Name = StrongType<std::string, struct NameTag_>;
using Street1 = StrongType<std::string, struct Street1Tag_>;
using Street2 = StrongType<std::string, struct Street2Tag_>;
using Street3 = StrongType<std::string, struct Street3Tag_>;
using City = StrongType<std::string, struct CityTag_>;
using StateOrProvince = StrongType<std::string, struct StateOrProvinceTag_>;
using PostalCode = StrongType<std::string, struct PostalCodeTag_>;
using CountryCode = StrongType<std::string, struct CountryCodeTag_>;
using Birthdate = StrongType<std::string, struct BirthdateTag_>;
using LogEntryId = StrongType<std::int64_t, struct LogEntryIdTag_>;
using RegistrarHandle = StrongType<std::string, struct RegistrarHandleTag_>;

struct AttachedContactData
{
    ContactId contact_id;
    ContactHandle contact_handle;
    bool name_change_to_prohibit;
    bool organization_change_to_prohibit;
    bool ident_change_to_prohibit;
    bool permanent_address_change_to_prohibit;
    bool name_change_requested;
    bool ident_change_requested;
    bool permanent_address_change_requested;
};

using AttachedContacts = std::vector<AttachedContactData>;

AttachedContacts get_attached_contacts(
        const LibPg::PgTransaction& tx,
        const IdentityProvider& provider,
        const Subject& subject,
        const Optional<Name>& name,
        const Optional<Street1>& street1,
        const Optional<Street2>& street2,
        const Optional<Street3>& street3,
        const Optional<City>& city,
        const Optional<StateOrProvince>& state_or_province,
        const Optional<PostalCode>& postal_code,
        const Optional<CountryCode>& country_code,
        const boost::optional<boost::gregorian::date>& birthdate)
{
    try
    {
        const auto dbres = exec(
                tx,
                LibPg::make_query(
                        "WITH c AS ("
                            "SELECT c.id, "
                                   "COALESCE(LOWER(REGEXP_REPLACE(TRIM(c.name),'\\s\\s+',' ','g')),'') AS name, "
                                   "COALESCE(LOWER(REGEXP_REPLACE(TRIM(c.street1),'\\s\\s+',' ','g')),'') AS street1, "
                                   "COALESCE(LOWER(REGEXP_REPLACE(TRIM(c.street2),'\\s\\s+',' ','g')),'') AS street2, "
                                   "COALESCE(LOWER(REGEXP_REPLACE(TRIM(c.street3),'\\s\\s+',' ','g')),'') AS street3, "
                                   "COALESCE(LOWER(REGEXP_REPLACE(TRIM(c.city),'\\s\\s+',' ','g')),'') AS city, "
                                   "COALESCE(LOWER(REGEXP_REPLACE(TRIM(c.stateorprovince),'\\s\\s+',' ','g')),'') AS stateorprovince, "
                                   "COALESCE(LOWER(REGEXP_REPLACE(TRIM(c.postalcode),'\\s\\s+',' ','g')),'') AS postalcode, "
                                   "COALESCE(LOWER(REGEXP_REPLACE(TRIM(c.country),'\\s\\s+',' ','g')),'') AS country, "
                                   "COALESCE((SELECT TRIM(c.ssn) FROM enum_ssntype WHERE type = 'BIRTHDAY' AND c.ssntype = id),'') AS birthdate, "
                                   "lock_object_state_request_lock(c.id) "
                            "FROM contact_identity ci "
                            "JOIN contact c ON c.id = ci.contact_id "
                            "WHERE ci.identity_provider = ") << LibPg::parameter<IdentityProvider>().as_varchar() << " AND "
                                  "ci.subject = " << LibPg::parameter<Subject>().as_varchar() << " AND "
                                  "ci.valid_to IS NULL "
                            "ORDER BY c.id "
                            "FOR UPDATE) "
                        "SELECT" << LibPg::item<ContactId>() << "obr.id"
                                 << LibPg::item<ContactHandle>() << "obr.name"
                                 << LibPg::item<NameChangeProhibited>() << "COALESCE(BOOL_OR("
                                        "eos.name = " << LibPg::parameter<ContactNameChangeProhibitedFlag>().as_varchar() << "), False)"
                                 << LibPg::item<OrganizationChangeProhibited>() << "COALESCE(BOOL_OR("
                                        "eos.name = " << LibPg::parameter<ContactOrganizationChangeProhibitedFlag>().as_varchar() << "), False)"
                                 << LibPg::item<IdentChangeProhibited>() << "COALESCE(BOOL_OR("
                                        "eos.name = " << LibPg::parameter<ContactIdentChangeProhibitedFlag>().as_varchar() << "), False)"
                                 << LibPg::item<PermanentAddressChangeProhibited>() << "COALESCE(BOOL_OR("
                                        "eos.name = " << LibPg::parameter<ContactPermanentAddressChangeProhibitedFlag>().as_varchar() << "), False)"
                                 << LibPg::item<NameChangeRequested>() << "COALESCE(c.name != LOWER(REGEXP_REPLACE(TRIM(" << LibPg::parameter<Optional<Name>>().as_varchar() << "),'\\s\\s+',' ','g')), False)"
                                 << LibPg::item<PermanentAddressChangeRequested>() << "COALESCE("
                                        "c.street1 != LOWER(REGEXP_REPLACE(TRIM(" << LibPg::parameter<Optional<Street1>>().as_varchar() << "),'\\s\\s+',' ','g')) OR "
                                        "c.street2 != LOWER(REGEXP_REPLACE(TRIM(" << LibPg::parameter<Optional<Street2>>().as_varchar() << "),'\\s\\s+',' ','g')) OR "
                                        "c.street3 != LOWER(REGEXP_REPLACE(TRIM(" << LibPg::parameter<Optional<Street3>>().as_varchar() << "),'\\s\\s+',' ','g')) OR "
                                        "c.city != LOWER(REGEXP_REPLACE(TRIM(" << LibPg::parameter<Optional<City>>().as_varchar() << "),'\\s\\s+',' ','g')) OR "
                                        "c.stateorprovince != LOWER(REGEXP_REPLACE(TRIM(" << LibPg::parameter<Optional<StateOrProvince>>().as_varchar() << "),'\\s\\s+',' ','g')) OR "
                                        "c.postalcode != LOWER(REGEXP_REPLACE(TRIM(" << LibPg::parameter<Optional<PostalCode>>().as_varchar() << "),'\\s\\s+',' ','g')) OR "
                                        "c.country != LOWER(REGEXP_REPLACE(TRIM(" << LibPg::parameter<Optional<CountryCode>>().as_varchar() << "),'\\s\\s+',' ','g')), False)"
                                 << LibPg::item<Birthdate>() << "c.birthdate "
                        "FROM c "
                        "JOIN object_registry obr ON obr.id = c.id "
                        "LEFT JOIN object_state_request osr ON osr.object_id = obr.id AND "
                                                              "osr.valid_from <= NOW() AND "
                                                              "osr.valid_to IS NULL AND "
                                                              "osr.canceled IS NULL AND "
                                                              "osr.state_id IN (SELECT id "
                                                                               "FROM enum_object_states "
                                                                               "WHERE name IN (" <<
                                                                    LibPg::parameter<ContactNameChangeProhibitedFlag>().as_varchar() << ", " <<
                                                                    LibPg::parameter<ContactOrganizationChangeProhibitedFlag>().as_varchar() << ", " <<
                                                                    LibPg::parameter<ContactIdentChangeProhibitedFlag>().as_varchar() << ", " <<
                                                                    LibPg::parameter<ContactPermanentAddressChangeProhibitedFlag>().as_varchar() << ")) "
                        "LEFT JOIN enum_object_states eos ON eos.id = osr.state_id "
                        "GROUP BY obr.id,"
                                 "c.id,"
                                 "c.name,"
                                 "c.street1,"
                                 "c.street2,"
                                 "c.street3,"
                                 "c.city,"
                                 "c.stateorprovince,"
                                 "c.postalcode,"
                                 "c.country,"
                                 "c.birthdate "
                        "ORDER BY obr.id",
                {provider,
                 subject,
                 name,
                 street1,
                 street2,
                 street3,
                 city,
                 state_or_province,
                 postal_code,
                 country_code,
                 ContactNameChangeProhibitedFlag{},
                 ContactOrganizationChangeProhibitedFlag{},
                 ContactIdentChangeProhibitedFlag{},
                 ContactPermanentAddressChangeProhibitedFlag{}});
        AttachedContacts result;
        for (auto&& row : dbres)
        {
            AttachedContactData contact;
            contact.contact_id = row.get<ContactId>();
            contact.contact_handle = row.get<ContactHandle>();
            contact.name_change_to_prohibit = !*row.get<NameChangeProhibited>() && (name != Name::nullopt);
            contact.organization_change_to_prohibit = !*row.get<OrganizationChangeProhibited>();
            contact.ident_change_to_prohibit = !*row.get<IdentChangeProhibited>() && (birthdate != boost::none);
            contact.permanent_address_change_to_prohibit = !*row.get<PermanentAddressChangeProhibited>() &&
                    (street1 != Street1::nullopt ||
                     street2 != Street2::nullopt ||
                     street3 != Street3::nullopt ||
                     city != City::nullopt ||
                     state_or_province != StateOrProvince::nullopt ||
                     postal_code != PostalCode::nullopt ||
                     country_code != CountryCode::nullopt);
            contact.name_change_requested = *row.get<NameChangeRequested>();
            contact.ident_change_requested = [&]()
            {
                if (birthdate == boost::none)
                {
                    return false;
                }
                const auto current_birthdate = *row.get<Birthdate>();
                if (current_birthdate.empty())
                {
                    return true;
                }
                try
                {
                    const auto current_birthdate_value = Util::birthdate_from_string_to_date(current_birthdate);
                    return current_birthdate_value != *birthdate;
                }
                catch (...)
                {
                    return true;
                }
            }();
            contact.permanent_address_change_requested = *row.get<PermanentAddressChangeRequested>();
            result.push_back(std::move(contact));
        }
        return result;
    }
    catch (const std::exception& e)
    {
        LIBLOG_ERROR("exception caught: {}", e.what());
        throw;
    }
    catch (...)
    {
        LIBLOG_ERROR("unexpected exception caught");
        throw;
    }
}

boost::gregorian::date to_boost_date(const Unwrapper::Date& date)
{
    return boost::gregorian::date{boost::gregorian::date::ymd_type{
            static_cast<short unsigned>(date.get_year()),
            static_cast<short unsigned>(date.get_month()),
            static_cast<short unsigned>(date.get_day())}};
}

void set_name(const Unwrapper::IdentityData& data, LibFred::UpdateContactById& update_op)
{
    if (data.has_name())
    {
        update_op.set_name(data.get_name());
    }
}

void set_birthdate(const Unwrapper::IdentityData& data, LibFred::UpdateContactById& update_op)
{
    if (data.has_birthdate())
    {
        const auto birthdate = to_boost_date(data.get_birthdate());
        update_op.set_personal_id(
                LibFred::PersonalIdUnion::get_BIRTHDAY(to_iso_extended_string(birthdate)));
    }
}

void set_place(const Unwrapper::IdentityData& data, LibFred::UpdateContactById& update_op)
{
    if (data.has_address())
    {
        const auto address = data.get_address();
        const auto streets = address.get_street();
        LibFred::Contact::PlaceAddress new_place;
        if (0 < streets.size())
        {
            new_place.street1 = streets[0];
            if (1 < streets.size())
            {
                new_place.street2 = streets[1];
                if (2 < streets.size())
                {
                    new_place.street3 = streets[2];
                }
            }
        }
        new_place.city = address.get_city();
        new_place.stateorprovince = address.get_state_or_province();
        new_place.postalcode = address.get_postal_code();
        new_place.country = address.get_country_code();
        update_op.set_place(new_place);
    }
}

void set_log_entry_id(const Optional<LogEntryId>& log_entry_id, LibFred::UpdateContactById& update_op)
{
    if (log_entry_id != LogEntryId::nullopt)
    {
        update_op.set_logd_request_id(**log_entry_id);
    }
}

void create_poll_message_on_update(const LibPg::PgRwTransaction& tx, const ContactHistoryId& contact_history_id)
{
    try
    {
        LibFred::Poll::CreateUpdateObjectPollMessage{}.exec(tx, *contact_history_id);
    }
    catch (const LibFred::OperationException& e)
    {
        LIBLOG_ERROR("CreateUpdateObjectPollMessage thrown OperationException: {}", e.what());
    }
    catch (const LibFred::InternalError& e)
    {
        LIBLOG_ERROR("CreateUpdateObjectPollMessage thrown InternalError: {}", e.what());
    }
    catch (const std::exception& e)
    {
        LIBLOG_ERROR("CreateUpdateObjectPollMessage thrown unexpected exception: {}", e.what());
    }
    catch (...)
    {
        LIBLOG_ERROR("CreateUpdateObjectPollMessage thrown unexpected exception");
    }
}

ContactHistoryId update_contact(
        const LibPg::PgRwTransaction& tx,
        const ContactId& contact_id,
        const Unwrapper::IdentityData& data,
        const RegistrarHandle& update_by_registrar,
        const Optional<LogEntryId>& log_entry_id)
{
    auto update_op = LibFred::UpdateContactById{*contact_id, *update_by_registrar};
    set_name(data, update_op);
    set_birthdate(data, update_op);
    set_place(data, update_op);
    set_log_entry_id(log_entry_id, update_op);
    try
    {
        auto contact_history_id = ContactHistoryId{update_op.exec(tx)};
        create_poll_message_on_update(tx, contact_history_id);
        return contact_history_id;
    }
    catch (const LibFred::UpdateContactById::ExceptionType& e)
    {
        if (e.is_set_unknown_contact_id())
        {
            struct ContactDoesNotExist : Exception
            {
                const char* what() const noexcept override { return "contact to update not found"; }
            };
            throw ContactDoesNotExist{};
        }
        throw;
    }
}

template <typename Flag>
void set(ContactState& state, bool& sum, bool value)
{
    std::get<Flag>(state) = Flag{value};
    sum |= value;
}

RegistrarHandle get_registrar()
{
    if (Cfg::Options::get().fred.registrar_originator != boost::none)
    {
        return RegistrarHandle{*Cfg::Options::get().fred.registrar_originator};
    }
    struct MissingOption : Exception
    {
        const char* what() const noexcept override { return "missing registrar option"; }
    };
    throw MissingOption{};
}

}//namespace Fred::Identity::Impl::Lib::{anonymous}

void update_identity(const LibPg::PgRwTransaction& tx, const Unwrapper::UpdateIdentityRequest& request)
{
    const auto streets = request.get_identity_data().has_address() ? request.get_identity_data().get_address().get_street()
                                                                   : std::vector<std::string>{};
    const auto has_street = [&](std::size_t idx) { return idx < (streets.size() + 1); };
    const auto attached_contacts = get_attached_contacts(
            tx,
            IdentityProvider{request.get_identity().get_identity_provider().get_value()},
            Subject{request.get_identity().get_subject().get_value()},
            Optional<Name>{request.get_identity_data().has_name(), [&]() { return Name{request.get_identity_data().get_name()}; }},
            Optional<Street1>{has_street(1), [&]() { return Street1{streets[0]}; }},
            Optional<Street2>{has_street(2), [&]() { return Street2{streets[1]}; }},
            Optional<Street3>{has_street(3), [&]() { return Street3{streets[2]}; }},
            Optional<City>{request.get_identity_data().has_address(), [&]() { return City{request.get_identity_data().get_address().get_city()}; }},
            Optional<StateOrProvince>{request.get_identity_data().has_address(), [&]() { return StateOrProvince{request.get_identity_data().get_address().get_state_or_province()}; }},
            Optional<PostalCode>{request.get_identity_data().has_address(), [&]() { return PostalCode{request.get_identity_data().get_address().get_postal_code()}; }},
            Optional<CountryCode>{request.get_identity_data().has_address(), [&]() { return CountryCode{request.get_identity_data().get_address().get_country_code()}; }},
            request.get_identity_data().has_birthdate() ? to_boost_date(request.get_identity_data().get_birthdate()) : boost::optional<boost::gregorian::date>{});
    const auto log_entry_id = Optional<LogEntryId>{
            request.has_log_entry_id(),
            [&]() { return LogEntryId{request.get_log_entry_id().get_value()}; }};
    std::for_each(begin(attached_contacts), end(attached_contacts), [&](auto&& contact)
            {
                ContactState state;
                const bool to_prohibit = [&]()
                {
                    bool result = false;
                    set<ContactStateFlag::ServerContactNameChangeProhibited>(state, result, contact.name_change_to_prohibit);
                    set<ContactStateFlag::ServerContactOrganizationChangeProhibited>(state, result, contact.organization_change_to_prohibit);
                    set<ContactStateFlag::ServerContactIdentChangeProhibited>(state, result, contact.ident_change_to_prohibit);
                    set<ContactStateFlag::ServerContactPermanentAddressChangeProhibited>(state, result, contact.permanent_address_change_to_prohibit);
                    return result;
                }();
                if (to_prohibit)
                {
                    create_contact_state_request(tx, contact.contact_id, state);
                }
                if (contact.name_change_requested ||
                    contact.ident_change_requested ||
                    contact.permanent_address_change_requested)
                {
                    update_contact(
                            tx,
                            contact.contact_id,
                            request.get_identity_data(),
                            get_registrar(),
                            log_entry_id);
                }
            });
}

}//namespace Fred::Identity::Impl::Lib
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred
