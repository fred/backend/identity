/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CREATE_CONTACT_STATE_REQUEST_HH_BC58D5E7FE45E123888655AD302DFEDF//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define CREATE_CONTACT_STATE_REQUEST_HH_BC58D5E7FE45E123888655AD302DFEDF

#include "src/impl/strong_type.hh"
#include "src/impl/lib/contact_state_flags.hh"

#include "libpg/pg_rw_transaction.hh"

#include <tuple>

namespace Fred {
namespace Identity {
namespace Impl {
namespace Lib {

using ContactState = std::tuple<
        ContactStateFlag::ServerContactNameChangeProhibited,
        ContactStateFlag::ServerContactOrganizationChangeProhibited,
        ContactStateFlag::ServerContactIdentChangeProhibited,
        ContactStateFlag::ServerContactPermanentAddressChangeProhibited>;

using ContactId = StrongType<std::uint64_t, struct ContactIdTag_>;

void create_contact_state_request(
        const LibPg::PgRwTransaction& tx,
        const ContactId& contact_id,
        const ContactState& contact_state);

}//namespace Fred::Identity::Impl::Lib
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#endif//CREATE_CONTACT_STATE_REQUEST_HH_BC58D5E7FE45E123888655AD302DFEDF
