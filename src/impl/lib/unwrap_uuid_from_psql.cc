/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/lib/unwrap_uuid_from_psql.hh"
#include "src/impl/exceptions.hh"

#include <boost/uuid/string_generator.hpp>

#include <stdexcept>


namespace boost {
namespace uuids {

uuid unwrap_from_psql_representation(const uuid&, const char* str)
{
    try
    {
        return string_generator{}(str);
    }
    catch (const std::runtime_error&)
    {
        struct ConversionFailure : Fred::Identity::Impl::InvalidUuid
        {
            const char* what() const noexcept override { return "unable to convert string to uuid"; }
        };
        throw ConversionFailure{};
    }
}

}//namespace boost::uuids
}//namespace boost
