/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/lib/create_contact_state_request.hh"
#include "src/impl/exceptions.hh"

#include "libfred/object_state/create_object_state_request_id.hh"
#include "libfred/object_state/lock_object_state_request_lock.hh"
#include "libfred/object_state/perform_object_state_request.hh"

#include "liblog/liblog.hh"

#include "libpg/query.hh"
#include "libpg/sql_state/code.hh"

#include <boost/uuid/uuid_io.hpp>

#include <spdlog/fmt/fmt.h>

#include <sstream>
#include <string>
#include <tuple>
#include <utility>


namespace Fred {
namespace Identity {
namespace Impl {
namespace Lib {

namespace {

class ContactStateFlagsNotAvailable : public Exception
{
public:
    ContactStateFlagsNotAvailable(const std::vector<std::string>& flags)
        : Exception{},
          msg_{"contact state flags [" + to_string(flags) + "] not available"}
    { }
    const char* what() const noexcept override
    {
        return msg_.c_str();
    }
private:
    static std::string to_string(const std::vector<std::string>& flags)
    {
        std::string result;
        std::for_each(begin(flags), end(flags), [&](auto&& str)
        {
            if (result.empty())
            {
                result = str;
            }
            else
            {
                result.append(", " + str);
            }
        });
        return result;
    }
    std::string msg_;
};

template <typename Flag>
void append(LibFred::StatusList& state, const Flag& flag)
{
    if (*flag)
    {
        state.insert(flag_name<Flag>());
    }
}

}//namespace Fred::Identity::Impl::Lib::{anonymous}

void create_contact_state_request(
        const LibPg::PgRwTransaction& tx,
        const ContactId& contact_id,
        const ContactState& contact_state)
{
    try
    {
        LibFred::StatusList state;
        append(state, std::get<ContactStateFlag::ServerContactNameChangeProhibited>(contact_state));
        append(state, std::get<ContactStateFlag::ServerContactOrganizationChangeProhibited>(contact_state));
        append(state, std::get<ContactStateFlag::ServerContactIdentChangeProhibited>(contact_state));
        append(state, std::get<ContactStateFlag::ServerContactPermanentAddressChangeProhibited>(contact_state));
        if (state.empty())
        {
            return;
        }
        LibFred::LockObjectStateRequestLock{*contact_id}.exec(tx);
        LibFred::CreateObjectStateRequestId{*contact_id, state}.exec(tx);
        LibFred::PerformObjectStateRequest{*contact_id}.exec(tx);
    }
    catch (const LibFred::CreateObjectStateRequestId::Exception& e)
    {
        if (e.is_set_vector_of_state_not_found())
        {
            throw ContactStateFlagsNotAvailable{e.get_vector_of_state_not_found()};
        }
        if (e.is_set_object_id_not_found())
        {
            struct ContactNotFound : ContactDoesNotExist
            {
                const char* what() const noexcept override
                {
                    return "contact not found by id";
                }
            };
            throw ContactNotFound{};
        }
        throw;
    }
}

}//namespace Fred::Identity::Impl::Lib
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred
