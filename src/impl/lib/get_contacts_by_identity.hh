/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_CONTACTS_BY_IDENTITY_HH_547FC402CF4047B6755FB2F070065EE6//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define GET_CONTACTS_BY_IDENTITY_HH_547FC402CF4047B6755FB2F070065EE6

#include "src/impl/unwrapper/get_contacts_by_identity_request.hh"
#include "src/impl/wrapper/get_contacts_by_identity_reply.hh"

#include "libpg/pg_transaction.hh"

namespace Fred {
namespace Identity {
namespace Impl {
namespace Lib {

void get_contacts_by_identity(const LibPg::PgTransaction& tx, const Unwrapper::GetContactsByIdentityRequest& request, const Wrapper::GetContactsByIdentityReply& reply);

}//namespace Fred::Identity::Impl::Lib
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#endif//GET_CONTACTS_BY_IDENTITY_HH_547FC402CF4047B6755FB2F070065EE6
