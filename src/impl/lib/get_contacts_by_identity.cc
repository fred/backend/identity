/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/lib/get_contacts_by_identity.hh"
#include "src/impl/lib/unwrap_uuid_from_psql.hh"
#include "src/impl/exceptions.hh"
#include "src/impl/strong_type.hh"
#include "liblog/liblog.hh"

#include "libpg/query.hh"

#include <stdexcept>
#include <utility>


namespace Fred {
namespace Identity {
namespace Impl {
namespace Lib {

namespace  {

using ContactUuid = StrongType<boost::uuids::uuid, struct ContactUuidTag_>;
using IdentityProvider = StrongType<std::string, struct IdentityProviderTag_>;
using Subject = StrongType<std::string, struct SubjectTag_>;
using ContactId = StrongType<std::uint64_t, struct ContactIdTag_>;
using IdentityId = StrongType<std::uint64_t, struct IdentityIdTag_>;
using ContactHandle = StrongType<std::string, struct ContactHandleTag_>;

}//namespace Fred::Identity::Impl::Lib::{anonymous}

void get_contacts_by_identity(const LibPg::PgTransaction& tx, const Unwrapper::GetContactsByIdentityRequest& request, const Wrapper::GetContactsByIdentityReply& reply)
{
    try
    {
        const auto dbres = exec(
                tx,
                LibPg::make_query(
                        "SELECT") << LibPg::item<ContactUuid>() << "obr.uuid"
                                  << LibPg::item<ContactId>() << "obr.id"
                                  << LibPg::item<ContactHandle>() << "obr.name "
                        "FROM object_registry obr "
                        "JOIN contact_identity ci ON ci.contact_id = obr.id "
                        "WHERE obr.erdate IS NULL AND "
                              "obr.type = get_object_type_id('contact') AND "
                              "ci.identity_provider = " << LibPg::parameter<IdentityProvider>().as_varchar() << " AND "
                              "ci.subject = " << LibPg::parameter<Subject>().as_varchar() << " AND "
                              "ci.valid_to IS NULL "
                        "ORDER BY obr.id",
                IdentityProvider{request.get_identity().get_identity_provider().get_value()},
                Subject{request.get_identity().get_subject().get_value()});
        const auto contacts = reply.set_data().set_contact_ids();
        contacts.reserve(*dbres.size());
        for (auto&& row : dbres)
        {
            const auto contact = contacts.add();
            contact.set_uuid().set_value(*row.get<ContactUuid>());
            contact.set_numeric_id(*row.get<ContactId>());
            contact.set_handle(*row.get<ContactHandle>());
        }
    }
    catch (const std::exception& e)
    {
        LIBLOG_ERROR("exception caught: {}", e.what());
        throw;
    }
    catch (...)
    {
        LIBLOG_ERROR("unexpected exception caught");
        throw;
    }
}

}//namespace Fred::Identity::Impl::Lib
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred
