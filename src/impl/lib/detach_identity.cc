/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/lib/detach_identity.hh"
#include "src/impl/lib/create_contact_state_request.hh"
#include "src/impl/lib/wrap_uuid_into_psql.hh"
#include "src/impl/exceptions.hh"
#include "src/impl/strong_type.hh"

#include "libfred/object_state/perform_object_state_request.hh"

#include "liblog/liblog.hh"

#include "libpg/pg_ro_transaction.hh"
#include "libpg/query.hh"

#include <boost/uuid/uuid_io.hpp>

#include <tuple>
#include <utility>


namespace Fred {
namespace Identity {
namespace Impl {
namespace Lib {

namespace  {

using ContactUuid = StrongType<boost::uuids::uuid, struct ContactUuidTag_>;
using IdentityProvider = StrongType<std::string, struct IdentityProviderTag_>;
using Subject = StrongType<std::string, struct SubjectTag_>;
using ContactId = StrongType<std::uint64_t, struct ContactIdTag_>;
using IdentityId = StrongType<std::uint64_t, struct IdentityIdTag_>;
using ContactHandle = StrongType<std::string, struct ContactHandleTag_>;

decltype(auto) detach_identity(
        const LibPg::PgRwTransaction& tx,
        const ContactUuid& contact_uuid,
        const IdentityProvider& provider,
        const Subject& subject)
{
    const auto dbres = exec(
            tx,
            LibPg::make_query(
                    "WITH "
                    "to_detach AS ("
                        "SELECT id AS contact_id, "
                               "name AS contact_handle, "
                               "lock_object_state_request_lock(id::bigint) "
                        "FROM object_registry "
                        "WHERE uuid = ") << LibPg::parameter<ContactUuid>().as_type("UUID") << " AND "
                              "type = get_object_type_id('contact') AND "
                              "erdate IS NULL), "
                    "cancel_object_state_request AS ("
                        "UPDATE object_state_request "
                        "SET canceled = NOW() "
                        "WHERE object_id = (SELECT contact_id FROM to_detach) AND "
                              "canceled IS NULL AND "
                              "state_id IN (SELECT id FROM enum_object_states WHERE name IN (" <<
                                    LibPg::parameter<ContactNameChangeProhibitedFlag>().as_text() << ", " <<
                                    LibPg::parameter<ContactOrganizationChangeProhibitedFlag>().as_text() << ", " <<
                                    LibPg::parameter<ContactIdentChangeProhibitedFlag>().as_text() << ", " <<
                                    LibPg::parameter<ContactPermanentAddressChangeProhibitedFlag>().as_text() << "))), "
                    "detach_identity AS ("
                        "UPDATE contact_identity "
                        "SET valid_to = CURRENT_TIMESTAMP "
                        "WHERE contact_id = (SELECT contact_id FROM to_detach) AND "
                              "identity_provider = " << LibPg::parameter<IdentityProvider>().as_varchar() << " AND "
                              "subject = " << LibPg::parameter<Subject>().as_varchar() << " AND "
                              "valid_to IS NULL "
                        "RETURNING id, contact_id) "
                    "SELECT" << LibPg::nullable_item<IdentityId>() << "detach_identity.id"
                             << LibPg::item<ContactId>() << "to_detach.contact_id"
                             << LibPg::item<ContactHandle>() << "to_detach.contact_handle "
                    "FROM to_detach "
                    "LEFT JOIN detach_identity ON detach_identity.contact_id = to_detach.contact_id",
            provider,
            subject,
            contact_uuid,
            ContactNameChangeProhibitedFlag{},
            ContactOrganizationChangeProhibitedFlag{},
            ContactIdentChangeProhibitedFlag{},
            ContactPermanentAddressChangeProhibitedFlag{});
    if (dbres.empty())
    {
        struct ContactNotFound : ContactDoesNotExist
        {
            const char* what() const noexcept override { return "given contact was not found"; }
        };
        throw ContactNotFound{};
    }
    if (LibPg::RowIndex{1} < dbres.size())
    {
        struct UnexpectedDatabaseResult : Exception
        {
            const char* what() const noexcept override { return "too many identities was found"; }
        };
        throw UnexpectedDatabaseResult{};
    }
    if (dbres.front().is_null<IdentityId>())
    {
        struct IdentityNotFound : IdentityDoesNotExist
        {
            const char* what() const noexcept override { return "no identity was found"; }
        };
        throw IdentityNotFound{};
    }
    auto result = std::make_tuple(dbres.front().get_nullable<IdentityId>(),
                                  dbres.front().get<ContactId>(),
                                  dbres.front().get<ContactHandle>());
    LibFred::PerformObjectStateRequest{*std::get<ContactId>(result)}.exec(tx);
    return result;
}

}//namespace Fred::Identity::Impl::Lib::{anonymous}

void detach_identity(const LibPg::PgRwTransaction& tx, const Unwrapper::DetachIdentityRequest& request)
{
    try
    {
        const auto removed = [&]()
        {
            return detach_identity(
                    tx,
                    ContactUuid{request.get_contact_id().get_uuid().get_value()},
                    IdentityProvider{request.get_identity().get_identity_provider().get_value()},
                    Subject{request.get_identity().get_subject().get_value()});
        }();
        LIBLOG_DEBUG("Identity {} removed from the contact ({}, '{}')", std::get<IdentityId>(removed),
                                                                        std::get<ContactId>(removed),
                                                                        std::get<ContactHandle>(removed));
    }
    catch (const NotFound& e)
    {
        LIBLOG_INFO("identity not found: {}", e.what());
        throw;
    }
    catch (const std::exception& e)
    {
        LIBLOG_ERROR("exception caught: {}", e.what());
        throw;
    }
    catch (...)
    {
        LIBLOG_ERROR("unexpected exception caught");
        throw;
    }
}

}//namespace Fred::Identity::Impl::Lib
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred
