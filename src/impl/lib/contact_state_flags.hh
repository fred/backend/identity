/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CONTACT_STATE_FLAGS_HH_D9E67D24ED1AC42B6BF36A4C66C3AE94//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define CONTACT_STATE_FLAGS_HH_D9E67D24ED1AC42B6BF36A4C66C3AE94

#include "src/impl/strong_type.hh"

#include <string>

namespace Fred {
namespace Identity {
namespace Impl {
namespace Lib {

//reduced set of contact state flags
struct ContactStateFlag
{
    using ServerContactNameChangeProhibited = StrongType<bool, struct ServerContactNameChangeProhibitedTag_>;
    using ServerContactOrganizationChangeProhibited = StrongType<bool, struct ServerContactOrganizationChangeProhibitedTag_>;
    using ServerContactIdentChangeProhibited = StrongType<bool, struct ServerContactIdentChangeProhibitedTag_>;
    using ServerContactPermanentAddressChangeProhibited = StrongType<bool, struct ServerContactPermanentAddressChangeProhibitedTag_>;
};

template <typename Flag>
std::string flag_name();

template <>
std::string flag_name<ContactStateFlag::ServerContactNameChangeProhibited>();
template <>
std::string flag_name<ContactStateFlag::ServerContactOrganizationChangeProhibited>();
template <>
std::string flag_name<ContactStateFlag::ServerContactIdentChangeProhibited>();
template <>
std::string flag_name<ContactStateFlag::ServerContactPermanentAddressChangeProhibited>();

struct ContactNameChangeProhibitedFlag {};
struct ContactOrganizationChangeProhibitedFlag {};
struct ContactIdentChangeProhibitedFlag {};
struct ContactPermanentAddressChangeProhibitedFlag {};

std::string wrap_into_psql_representation(ContactNameChangeProhibitedFlag);
std::string wrap_into_psql_representation(ContactOrganizationChangeProhibitedFlag);
std::string wrap_into_psql_representation(ContactIdentChangeProhibitedFlag);
std::string wrap_into_psql_representation(ContactPermanentAddressChangeProhibitedFlag);

}//namespace Fred::Identity::Impl::Lib
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#endif//CONTACT_STATE_FLAGS_HH_D9E67D24ED1AC42B6BF36A4C66C3AE94
