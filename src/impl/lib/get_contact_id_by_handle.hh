/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_CONTACT_ID_BY_HANDLE_HH_D079857920A8604C728EB2472AF4193F//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define GET_CONTACT_ID_BY_HANDLE_HH_D079857920A8604C728EB2472AF4193F

#include "src/impl/unwrapper/get_contact_id_by_handle_request.hh"
#include "src/impl/wrapper/common_types.hh"

#include "libpg/pg_ro_transaction.hh"

namespace Fred {
namespace Identity {
namespace Impl {
namespace Lib {

template <typename Reply>
void get_contact_id_by_handle(const LibPg::PgRoTransaction& tx, const Unwrapper::GetContactIdByHandleRequest& request, Reply* response);

}//namespace Fred::Identity::Impl::Lib
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#endif//GET_CONTACT_ID_BY_HANDLE_HH_D079857920A8604C728EB2472AF4193F
