/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DETACH_IDENTITY_HH_5F5B2F4997402AA46D6803ED521DE573//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define DETACH_IDENTITY_HH_5F5B2F4997402AA46D6803ED521DE573

#include "src/impl/unwrapper/detach_identity_request.hh"

#include "libpg/pg_rw_transaction.hh"

namespace Fred {
namespace Identity {
namespace Impl {
namespace Lib {

void detach_identity(const LibPg::PgRwTransaction& tx, const Unwrapper::DetachIdentityRequest& request);

}//namespace Fred::Identity::Impl::Lib
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#endif//DETACH_IDENTITY_HH_5F5B2F4997402AA46D6803ED521DE573
