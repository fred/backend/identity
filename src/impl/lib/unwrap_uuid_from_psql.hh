/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UNWRAP_UUID_FROM_PSQL_HH_AA65F6030565218A6AAA5C7296C48E19//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define UNWRAP_UUID_FROM_PSQL_HH_AA65F6030565218A6AAA5C7296C48E19

#include <boost/uuid/uuid.hpp>

namespace boost {
namespace uuids {

// Unwrapper used in LibPg
uuid unwrap_from_psql_representation(const uuid&, const char* str);

}//namespace boost::uuids
}//namespace boost

#endif//UNWRAP_UUID_FROM_PSQL_HH_AA65F6030565218A6AAA5C7296C48E19
