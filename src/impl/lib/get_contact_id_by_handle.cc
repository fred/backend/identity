/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/lib/get_contact_id_by_handle.hh"
#include "src/impl/lib/unwrap_uuid_from_psql.hh"
#include "src/impl/exceptions.hh"
#include "src/impl/strong_type.hh"

#include "fred_api/identity/service_identity_manager_grpc.pb.h"

#include "liblog/liblog.hh"

#include "libpg/pg_ro_transaction.hh"
#include "libpg/query.hh"

#include <boost/uuid/uuid_io.hpp>

#include <tuple>
#include <utility>


namespace Fred {
namespace Identity {
namespace Impl {
namespace Lib {

namespace  {

using ContactUuid = StrongType<boost::uuids::uuid, struct ContactUuidTag_>;
using ContactId = StrongType<std::uint64_t, struct ContactIdTag_>;
using ContactHandle = StrongType<std::string, struct ContactHandleTag_>;

decltype(auto) get_contact_id_by_handle(
        const LibPg::PgRoTransaction& tx,
        const ContactHandle& contact_handle)
{
    const auto dbres = exec(
            tx,
            LibPg::make_query("SELECT") << LibPg::item<ContactId>() << "id"
                                        << LibPg::item<ContactUuid>() << "uuid"
                                        << LibPg::item<ContactHandle>() << "UPPER(name) "
                              "FROM object_registry "
                              "WHERE erdate IS NULL AND "
                                    "type=get_object_type_id('contact') AND "
                                    "name=UPPER(" << LibPg::parameter<ContactHandle>().as_text() << ")",
            contact_handle);
    if (dbres.empty())
    {
        struct ContactNotFound : ContactDoesNotExist
        {
            const char* what() const noexcept override { return "no contact has given handle"; }
        };
        throw ContactNotFound{};
    }
    if (LibPg::RowIndex{1} < dbres.size())
    {
        struct UnexpectedDatabaseResult : Exception
        {
            const char* what() const noexcept override { return "too many contacts was found"; }
        };
        throw UnexpectedDatabaseResult{};
    }
    return std::make_tuple(dbres.front().get<ContactId>(),
                           dbres.front().get<ContactUuid>(),
                           dbres.front().get<ContactHandle>());
}

}//namespace Fred::Identity::Impl::Lib::{anonymous}

template <>
void get_contact_id_by_handle(const LibPg::PgRoTransaction& tx, const Unwrapper::GetContactIdByHandleRequest& request, Api::GetContactIdByHandleReply* response)
{
    try
    {
        const auto contact_id = get_contact_id_by_handle(tx, ContactHandle{request.get_contact_handle().get_value()});
        LIBLOG_DEBUG("Contact ({}, {}, '{}')", std::get<ContactId>(contact_id), to_string(*std::get<ContactUuid>(contact_id)), std::get<ContactHandle>(contact_id));
        response->Clear();
        Wrapper::ContactId data{response->mutable_data()->mutable_contact_id()};
        data.set_numeric_id(*std::get<ContactId>(contact_id));
        data.set_uuid().set_value(*std::get<ContactUuid>(contact_id));
        data.set_handle(*std::get<ContactHandle>(contact_id));
    }
    catch (const ContactDoesNotExist& e)
    {
        LIBLOG_INFO("Contact '{}' not found: {}", request.get_contact_handle().get_value(), e.what());
        throw;
    }
    catch (const std::exception& e)
    {
        LIBLOG_ERROR("exception caught: {}", e.what());
        throw;
    }
    catch (...)
    {
        LIBLOG_ERROR("unexpected exception caught");
        throw;
    }
}

}//namespace Fred::Identity::Impl::Lib
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred
