/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UPDATE_IDENTITY_HH_3835C04289A9C10BCFE737ACC5ADAA11//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define UPDATE_IDENTITY_HH_3835C04289A9C10BCFE737ACC5ADAA11

#include "src/impl/unwrapper/update_identity_request.hh"

#include "libpg/pg_rw_transaction.hh"

namespace Fred {
namespace Identity {
namespace Impl {
namespace Lib {

void update_identity(const LibPg::PgRwTransaction& tx, const Unwrapper::UpdateIdentityRequest& request);

}//namespace Fred::Identity::Impl::Lib
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#endif//UPDATE_IDENTITY_HH_3835C04289A9C10BCFE737ACC5ADAA11
