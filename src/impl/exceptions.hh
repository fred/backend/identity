/*
 * Copyright (C) 2021-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EXCEPTIONS_HH_2579D905EF244726DA40C2A72A3E20DD//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define EXCEPTIONS_HH_2579D905EF244726DA40C2A72A3E20DD

#include <exception>

namespace Fred {
namespace Identity {
namespace Impl {

struct Exception : std::exception { };
struct InvalidArgument : Exception { };
struct InvalidUuid : Exception { };
struct AlreadyExists : Exception { };
struct NotFound : Exception { };
struct FailedPrecondition : Exception { };

struct IdentityAttached : AlreadyExists { };
struct ContactDoesNotExist : NotFound { };
struct HistoryIdMismatch : FailedPrecondition { };
struct IdentityDoesNotExist : NotFound { };
struct MissingContactId : InvalidArgument { };
struct ContactIsOrganization : FailedPrecondition { };

}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#endif//EXCEPTIONS_HH_2579D905EF244726DA40C2A72A3E20DD
