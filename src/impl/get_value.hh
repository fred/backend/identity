/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_VALUE_HH_AD0E72336C0C4B43538A7BEB72B09F8C//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define GET_VALUE_HH_AD0E72336C0C4B43538A7BEB72B09F8C

#include <tuple>

namespace Fred {
namespace Identity {
namespace Impl {

template <typename KeyType, typename ValueType> struct KeyValuePair;

template <typename KeyType, typename KeyValueMap> struct GetValue;

template <typename KeyType, typename ValueType, typename ...Pairs>
struct GetValue<KeyType, std::tuple<KeyValuePair<KeyType, ValueType>, Pairs...>>
{
    using Type = ValueType;
};

template <typename KeyType, typename FirstPair, typename ...Pairs>
struct GetValue<KeyType, std::tuple<FirstPair, Pairs...>>
{
    static_assert(0 < sizeof...(Pairs), "Corresponding ValueType not found");
    using Type = typename GetValue<KeyType, std::tuple<Pairs...>>::Type;
};

}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#endif//GET_VALUE_HH_AD0E72336C0C4B43538A7BEB72B09F8C
