/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef API_TYPE_GETTER_HH_52ED17366AE2E6DE0EA2417939686DFA//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define API_TYPE_GETTER_HH_52ED17366AE2E6DE0EA2417939686DFA

namespace Fred {
namespace Identity {
namespace Impl {

template <typename Unwrapper>
class ApiTypeGetter
{
protected:
    using ApiData = ApiTypeGetter;
    template <typename ApiType>
    explicit ApiTypeGetter(const ApiType& ref) noexcept;
    ApiTypeGetter(const ApiTypeGetter&) noexcept = default;
    template <typename ApiType>
    const ApiType& get() const noexcept;
public:
    ApiTypeGetter() = delete;
    ApiTypeGetter(ApiTypeGetter&&) = delete;
    ApiTypeGetter& operator=(const ApiTypeGetter&) = delete;
    ApiTypeGetter& operator=(ApiTypeGetter&&) = delete;
private:
    const void* const api_data_;
};

}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#endif//API_TYPE_GETTER_HH_52ED17366AE2E6DE0EA2417939686DFA
