/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef COMMON_TYPES_HH_3B113745FD36486EB084B38D839F1DF4//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define COMMON_TYPES_HH_3B113745FD36486EB084B38D839F1DF4

#include "src/impl/api_type_getter.hh"

#include <boost/uuid/uuid.hpp>

#include <cstdint>
#include <string>
#include <vector>

namespace Fred {
namespace Identity {
namespace Impl {
namespace Unwrapper {

class Uuid : private ApiTypeGetter<Uuid>
{
public:
    template <typename ApiType>
    explicit Uuid(const ApiType& ref) noexcept;
    boost::uuids::uuid get_value() const;
};

class ContactId : private ApiTypeGetter<ContactId>
{
public:
    template <typename ApiType>
    explicit ContactId(const ApiType& ref) noexcept;
    bool has_uuid() const;
    Uuid get_uuid() const;
    std::uint64_t get_numeric_id() const;
    const std::string& get_handle() const;
};

class ContactHandle : private ApiTypeGetter<ContactHandle>
{
public:
    template <typename ApiType>
    explicit ContactHandle(const ApiType& ref) noexcept;
    const std::string& get_value() const;
};

class ContactHistoryId : private ApiTypeGetter<ContactHistoryId>
{
public:
    template <typename ApiType>
    explicit ContactHistoryId(const ApiType& ref) noexcept;
    bool has_uuid() const;
    Uuid get_uuid() const;
};

class Identity : private ApiTypeGetter<Identity>
{
public:
    template <typename ApiType>
    explicit Identity(const ApiType& ref) noexcept;
    class IdentityProvider : private ApiTypeGetter<IdentityProvider>
    {
    public:
        template <typename ApiType>
        explicit IdentityProvider(const ApiType& ref) noexcept;
        const std::string& get_value() const;
    };
    class Subject : private ApiTypeGetter<Subject>
    {
    public:
        template <typename ApiType>
        explicit Subject(const ApiType& ref) noexcept;
        const std::string& get_value() const;
    };
    bool has_identity_provider() const;
    IdentityProvider get_identity_provider() const;
    bool has_subject() const;
    Subject get_subject() const;
};

class Date : private ApiTypeGetter<Date>
{
public:
    template <typename ApiType>
    explicit Date(const ApiType& ref) noexcept;
    std::int32_t get_year() const;
    std::int32_t get_month() const;
    std::int32_t get_day() const;
};

class PlaceAddress : private ApiTypeGetter<PlaceAddress>
{
public:
    template <typename ApiType>
    explicit PlaceAddress(const ApiType& ref) noexcept;
    std::vector<std::string> get_street() const;
    const std::string& get_city() const;
    const std::string& get_state_or_province() const;
    const std::string& get_postal_code() const;
    const std::string& get_country_code() const;
};

class IdentityData : private ApiTypeGetter<IdentityData>
{
public:
    template <typename ApiType>
    explicit IdentityData(const ApiType& ref) noexcept;
    bool has_name() const;
    const std::string& get_name() const;
    bool has_birthdate() const;
    Date get_birthdate() const;
    bool has_address() const;
    PlaceAddress get_address() const;
};

class LogEntryId : private ApiTypeGetter<LogEntryId>
{
public:
    template <typename ApiType>
    explicit LogEntryId(const ApiType& ref) noexcept;
    std::int64_t get_value() const;
};

}//namespace Fred::Identity::Impl::Unwrapper
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#endif//COMMON_TYPES_HH_3B113745FD36486EB084B38D839F1DF4
