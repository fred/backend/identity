/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_CONTACTS_BY_IDENTITY_REQUEST_HH_931E6017820557A1EDAA24D408CE749C//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define GET_CONTACTS_BY_IDENTITY_REQUEST_HH_931E6017820557A1EDAA24D408CE749C

#include "src/impl/unwrapper/common_types.hh"
#include "src/impl/api_type_getter.hh"

namespace Fred {
namespace Identity {
namespace Impl {
namespace Unwrapper {

class GetContactsByIdentityRequest : private ApiTypeGetter<GetContactsByIdentityRequest>
{
public:
    template <typename ApiType>
    explicit GetContactsByIdentityRequest(const ApiType& ref) noexcept;
    bool has_identity() const;
    Identity get_identity() const;
};

}//namespace Fred::Identity::Impl::Unwrapper
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#endif//GET_CONTACTS_BY_IDENTITY_REQUEST_HH_931E6017820557A1EDAA24D408CE749C
