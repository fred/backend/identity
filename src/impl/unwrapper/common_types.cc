/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/unwrapper/common_types.hh"
#include "src/impl/exceptions.hh"
#include "src/impl/get_value.hh"

#include "fred_api/identity/common_types.pb.h"

#include <boost/uuid/string_generator.hpp>

#include <type_traits>
#include <utility>

namespace Fred {
namespace Identity {
namespace Impl {
namespace Unwrapper {

namespace {

using ApiUnwrappers = std::tuple<KeyValuePair<Uuid, Api::Uuid>,
                                 KeyValuePair<ContactId, Api::ContactId>,
                                 KeyValuePair<ContactHandle, Api::ContactHandle>,
                                 KeyValuePair<ContactHistoryId, Api::ContactHistoryId>,
                                 KeyValuePair<Identity, Api::Identity>,
                                 KeyValuePair<Identity::IdentityProvider, Api::Identity::IdentityProvider>,
                                 KeyValuePair<Identity::Subject, Api::Identity::Subject>,
                                 KeyValuePair<Date, Api::Date>,
                                 KeyValuePair<PlaceAddress, Api::PlaceAddress>,
                                 KeyValuePair<IdentityData, Api::IdentityData>,
                                 KeyValuePair<LogEntryId, Api::LogEntryId>>;

template <typename UnwrapperType>
using GetApiT = typename GetValue<UnwrapperType, ApiUnwrappers>::Type;

}//namespace Fred::Identity::Impl::Unwrapper::{anonymous}

}//namespace Fred::Identity::Impl::Unwrapper
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#include "src/impl/api_type_getter_impl.hh" // must be included after GetApiT defining

namespace Fred {
namespace Identity {
namespace Impl {
namespace Unwrapper {

template <>
Uuid::Uuid(const Api::Uuid& ref) noexcept
    : ApiData{ref}
{ }

boost::uuids::uuid Uuid::get_value() const
{
    static const auto uuid_from_string = [](const std::string& str)
    {
        try
        {
            return boost::uuids::string_generator{}(str);
        }
        catch (const std::runtime_error&)
        {
            struct ConversionFailure : InvalidArgument
            {
                const char* what() const noexcept override { return "unable to convert string to uuid"; }
            };
            throw ConversionFailure{};
        }
    };
    return uuid_from_string(this->ApiData::get<Api::Uuid>().value());
}

template <>
ContactId::ContactId(const Api::ContactId& ref) noexcept
    : ApiData{ref}
{ }

bool ContactId::has_uuid() const
{
    return this->ApiData::get<Api::ContactId>().has_uuid();
}

Uuid ContactId::get_uuid() const
{
    return Uuid{this->ApiData::get<Api::ContactId>().uuid()};
}

std::uint64_t ContactId::get_numeric_id() const
{
    return this->ApiData::get<Api::ContactId>().numeric_id();
}

const std::string& ContactId::get_handle() const
{
    return this->ApiData::get<Api::ContactId>().handle();
}

template <>
ContactHandle::ContactHandle(const Api::ContactHandle& ref) noexcept
    : ApiData{ref}
{ }

const std::string& ContactHandle::get_value() const
{
    return this->ApiData::get<Api::ContactHandle>().value();
}

template <>
ContactHistoryId::ContactHistoryId(const Api::ContactHistoryId& ref) noexcept
    : ApiData{ref}
{ }

bool ContactHistoryId::has_uuid() const
{
    return this->ApiData::get<Api::ContactHistoryId>().has_uuid();
}

Uuid ContactHistoryId::get_uuid() const
{
    return Uuid{this->ApiData::get<Api::ContactHistoryId>().uuid()};
}

template <>
Identity::IdentityProvider::IdentityProvider(const Api::Identity::IdentityProvider& ref) noexcept
    : ApiData{ref}
{ }

const std::string& Identity::IdentityProvider::get_value() const
{
    return this->ApiData::get<Api::Identity::IdentityProvider>().value();
}

template <>
Identity::Subject::Subject(const Api::Identity::Subject& ref) noexcept
    : ApiData{ref}
{ }

const std::string& Identity::Subject::get_value() const
{
    return this->ApiData::get<Api::Identity::Subject>().value();
}

template <>
Identity::Identity(const Api::Identity& ref) noexcept
    : ApiData{ref}
{ }

bool Identity::has_identity_provider() const
{
    return this->ApiData::get<Api::Identity>().has_identity_provider();
}

Identity::IdentityProvider Identity::get_identity_provider() const
{
    return IdentityProvider{this->ApiData::get<Api::Identity>().identity_provider()};
}

bool Identity::has_subject() const
{
    return this->ApiData::get<Api::Identity>().has_subject();
}

Identity::Subject Identity::get_subject() const
{
    return Subject{this->ApiData::get<Api::Identity>().subject()};
}

template <>
Date::Date(const Api::Date& ref) noexcept
    : ApiData{ref}
{ }

std::int32_t Date::get_year() const
{
    return this->ApiData::get<Api::Date>().year();
}

std::int32_t Date::get_month() const
{
    return this->ApiData::get<Api::Date>().month();
}

std::int32_t Date::get_day() const
{
    return this->ApiData::get<Api::Date>().day();
}

template <>
PlaceAddress::PlaceAddress(const Api::PlaceAddress& ref) noexcept
    : ApiData{ref}
{ }

std::vector<std::string> PlaceAddress::get_street() const
{
    const auto& street = this->ApiData::get<Api::PlaceAddress>().street();
    const std::string* const* const begin = street.data();
    const std::string* const* const end = begin + street.size();
    std::vector<std::string> result{};
    result.reserve(street.size());
    std::transform(
            begin,
            end,
            std::back_inserter(result),
            [](const std::string* street_part) -> const std::string& { return *street_part; });
    return result;
}

const std::string& PlaceAddress::get_city() const
{
    return this->ApiData::get<Api::PlaceAddress>().city();
}

const std::string& PlaceAddress::get_state_or_province() const
{
    return this->ApiData::get<Api::PlaceAddress>().state_or_province();
}

const std::string& PlaceAddress::get_postal_code() const
{
    return this->ApiData::get<Api::PlaceAddress>().postal_code();
}

const std::string& PlaceAddress::get_country_code() const
{
    return this->ApiData::get<Api::PlaceAddress>().country_code();
}

template <>
IdentityData::IdentityData(const Api::IdentityData& ref) noexcept
    : ApiData{ref}
{ }

bool IdentityData::has_name() const
{
    return this->ApiData::get<Api::IdentityData>().has_name();
}

const std::string& IdentityData::get_name() const
{
    return this->ApiData::get<Api::IdentityData>().name();
}

bool IdentityData::has_birthdate() const
{
    return this->ApiData::get<Api::IdentityData>().has_birthdate();
}

Date IdentityData::get_birthdate() const
{
    return Date{this->ApiData::get<Api::IdentityData>().birthdate()};
}

bool IdentityData::has_address() const
{
    return this->ApiData::get<Api::IdentityData>().has_address();
}

PlaceAddress IdentityData::get_address() const
{
    return PlaceAddress{this->ApiData::get<Api::IdentityData>().address()};
}

template <>
LogEntryId::LogEntryId(const Api::LogEntryId& ref) noexcept
    : ApiData{ref}
{ }

std::int64_t LogEntryId::get_value() const
{
    return this->ApiData::get<Api::LogEntryId>().value();
}

}//namespace Fred::Identity::Impl::Unwrapper
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred
