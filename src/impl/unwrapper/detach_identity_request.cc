/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/unwrapper/detach_identity_request.hh"
#include "src/impl/get_value.hh"

#include "fred_api/identity/service_identity_manager_grpc.pb.h"

#include <type_traits>
#include <utility>

namespace Fred {
namespace Identity {
namespace Impl {
namespace Unwrapper {

namespace {

using ApiUnwrappers = std::tuple<KeyValuePair<DetachIdentityRequest, Api::DetachIdentityRequest>>;

template <typename UnwrapperType>
using GetApiT = typename GetValue<UnwrapperType, ApiUnwrappers>::Type;

}//namespace Fred::Identity::Impl::Unwrapper::{anonymous}

}//namespace Fred::Identity::Impl::Unwrapper
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#include "src/impl/api_type_getter_impl.hh" // must be included after GetApiT defining

namespace Fred {
namespace Identity {
namespace Impl {
namespace Unwrapper {

template <>
DetachIdentityRequest::DetachIdentityRequest(const Api::DetachIdentityRequest& ref) noexcept
    : ApiData{ref}
{ }

bool DetachIdentityRequest::has_contact_id() const
{
    return this->ApiData::get<Api::DetachIdentityRequest>().has_contact_id();
}

ContactId DetachIdentityRequest::get_contact_id() const
{
    return ContactId{this->ApiData::get<Api::DetachIdentityRequest>().contact_id()};
}

bool DetachIdentityRequest::has_identity() const
{
    return this->ApiData::get<Api::DetachIdentityRequest>().has_identity();
}

Identity DetachIdentityRequest::get_identity() const
{
    return Identity{this->ApiData::get<Api::DetachIdentityRequest>().identity()};
}

}//namespace Fred::Identity::Impl::Unwrapper
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred
