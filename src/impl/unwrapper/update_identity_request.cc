/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/unwrapper/update_identity_request.hh"
#include "src/impl/get_value.hh"

#include "fred_api/identity/service_identity_manager_grpc.pb.h"

#include <type_traits>
#include <utility>

namespace Fred {
namespace Identity {
namespace Impl {
namespace Unwrapper {

namespace {

using ApiUnwrappers = std::tuple<KeyValuePair<UpdateIdentityRequest, Api::UpdateIdentityRequest>>;

template <typename UnwrapperType>
using GetApiT = typename GetValue<UnwrapperType, ApiUnwrappers>::Type;

}//namespace Fred::Identity::Impl::Unwrapper::{anonymous}

}//namespace Fred::Identity::Impl::Unwrapper
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#include "src/impl/api_type_getter_impl.hh" // must be included after GetApiT defining

namespace Fred {
namespace Identity {
namespace Impl {
namespace Unwrapper {

template <>
UpdateIdentityRequest::UpdateIdentityRequest(const Api::UpdateIdentityRequest& ref) noexcept
    : ApiData{ref}
{ }

bool UpdateIdentityRequest::has_identity() const
{
    return this->ApiData::get<Api::UpdateIdentityRequest>().has_identity();
}

Identity UpdateIdentityRequest::get_identity() const
{
    return Identity{this->ApiData::get<Api::UpdateIdentityRequest>().identity()};
}

bool UpdateIdentityRequest::has_identity_data() const
{
    return this->ApiData::get<Api::UpdateIdentityRequest>().has_identity_data();
}

IdentityData UpdateIdentityRequest::get_identity_data() const
{
    return IdentityData{this->ApiData::get<Api::UpdateIdentityRequest>().identity_data()};
}

bool UpdateIdentityRequest::has_log_entry_id() const
{
    return this->ApiData::get<Api::UpdateIdentityRequest>().has_log_entry_id();
}

LogEntryId UpdateIdentityRequest::get_log_entry_id() const
{
    return LogEntryId{this->ApiData::get<Api::UpdateIdentityRequest>().log_entry_id()};
}

}//namespace Fred::Identity::Impl::Unwrapper
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred
