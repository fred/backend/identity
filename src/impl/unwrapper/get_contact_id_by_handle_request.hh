/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_CONTACT_ID_BY_HANDLE_REQUEST_HH_80D7892963CC45C611AE54F893B518A3//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define GET_CONTACT_ID_BY_HANDLE_REQUEST_HH_80D7892963CC45C611AE54F893B518A3

#include "src/impl/unwrapper/common_types.hh"
#include "src/impl/api_type_getter.hh"

namespace Fred {
namespace Identity {
namespace Impl {
namespace Unwrapper {

class GetContactIdByHandleRequest : private ApiTypeGetter<GetContactIdByHandleRequest>
{
public:
    template <typename ApiType>
    explicit GetContactIdByHandleRequest(const ApiType& ref) noexcept;
    bool has_contact_handle() const;
    ContactHandle get_contact_handle() const;
};

}//namespace Fred::Identity::Impl::Unwrapper
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#endif//GET_CONTACT_ID_BY_HANDLE_REQUEST_HH_80D7892963CC45C611AE54F893B518A3
