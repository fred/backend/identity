/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ATTACH_IDENTITY_REQUEST_HH_8644E80294629334C8E8256B17081507//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define ATTACH_IDENTITY_REQUEST_HH_8644E80294629334C8E8256B17081507

#include "src/impl/unwrapper/common_types.hh"
#include "src/impl/api_type_getter.hh"

#include <vector>

namespace Fred {
namespace Identity {
namespace Impl {
namespace Unwrapper {

class AttachIdentityRequest : private ApiTypeGetter<AttachIdentityRequest>
{
public:
    template <typename ApiType>
    explicit AttachIdentityRequest(const ApiType& ref) noexcept;
    bool has_contact_id() const;
    ContactId get_contact_id() const;
    bool has_contact_history_id() const;
    ContactHistoryId get_contact_history_id() const;
    bool has_identity() const;
    Identity get_identity() const;
    bool has_identity_data() const;
    IdentityData get_identity_data() const;
};

}//namespace Fred::Identity::Impl::Unwrapper
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#endif//ATTACH_IDENTITY_REQUEST_HH_8644E80294629334C8E8256B17081507
