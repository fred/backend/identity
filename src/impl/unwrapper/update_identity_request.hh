/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UPDATE_IDENTITY_REQUEST_HH_E76BA32DF1760CA47746D49F97036457//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define UPDATE_IDENTITY_REQUEST_HH_E76BA32DF1760CA47746D49F97036457

#include "src/impl/unwrapper/common_types.hh"
#include "src/impl/api_type_getter.hh"

namespace Fred {
namespace Identity {
namespace Impl {
namespace Unwrapper {

class UpdateIdentityRequest : private ApiTypeGetter<UpdateIdentityRequest>
{
public:
    template <typename ApiType>
    explicit UpdateIdentityRequest(const ApiType& ref) noexcept;
    bool has_identity() const;
    Identity get_identity() const;
    bool has_identity_data() const;
    IdentityData get_identity_data() const;
    bool has_log_entry_id() const;
    LogEntryId get_log_entry_id() const;
};

}//namespace Fred::Identity::Impl::Unwrapper
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#endif//UPDATE_IDENTITY_REQUEST_HH_E76BA32DF1760CA47746D49F97036457
