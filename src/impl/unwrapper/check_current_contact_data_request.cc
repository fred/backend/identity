/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/unwrapper/check_current_contact_data_request.hh"
#include "src/impl/get_value.hh"

#include "fred_api/identity/service_identity_checker_grpc.pb.h"

#include <type_traits>
#include <utility>

namespace Fred {
namespace Identity {
namespace Impl {
namespace Unwrapper {

namespace {

using ApiUnwrappers = std::tuple<KeyValuePair<CheckCurrentContactDataRequest, Api::CheckCurrentContactDataRequest>>;

template <typename UnwrapperType>
using GetApiT = typename GetValue<UnwrapperType, ApiUnwrappers>::Type;

}//namespace Fred::Identity::Impl::Unwrapper::{anonymous}

}//namespace Fred::Identity::Impl::Unwrapper
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#include "src/impl/api_type_getter_impl.hh" // must be included after GetApiT defining

namespace Fred {
namespace Identity {
namespace Impl {
namespace Unwrapper {

template <>
CheckCurrentContactDataRequest::CheckCurrentContactDataRequest(const Api::CheckCurrentContactDataRequest& ref) noexcept
    : ApiData{ref}
{ }

bool CheckCurrentContactDataRequest::has_contact_id() const
{
    return this->ApiData::get<Api::CheckCurrentContactDataRequest>().has_contact_id();
}

ContactId CheckCurrentContactDataRequest::get_contact_id() const
{
    return ContactId{this->ApiData::get<Api::CheckCurrentContactDataRequest>().contact_id()};
}

bool CheckCurrentContactDataRequest::has_contact_history_id() const
{
    return this->ApiData::get<Api::CheckCurrentContactDataRequest>().has_contact_history_id();
}

ContactHistoryId CheckCurrentContactDataRequest::get_contact_history_id() const
{
    return ContactHistoryId{this->ApiData::get<Api::CheckCurrentContactDataRequest>().contact_history_id()};
}

const std::string& CheckCurrentContactDataRequest::get_auth_info() const
{
    return this->ApiData::get<Api::CheckCurrentContactDataRequest>().auth_info();
}
const std::string& CheckCurrentContactDataRequest::get_name() const
{
    return this->ApiData::get<Api::CheckCurrentContactDataRequest>().name();
}

bool CheckCurrentContactDataRequest::has_address() const
{
    return this->ApiData::get<Api::CheckCurrentContactDataRequest>().has_address();
}

PlaceAddress CheckCurrentContactDataRequest::get_address() const
{
    return PlaceAddress{this->ApiData::get<Api::CheckCurrentContactDataRequest>().address()};
}

bool CheckCurrentContactDataRequest::has_birthdate() const
{
    return this->ApiData::get<Api::CheckCurrentContactDataRequest>().has_birthdate();
}

Date CheckCurrentContactDataRequest::get_birthdate() const
{
    return Date{this->ApiData::get<Api::CheckCurrentContactDataRequest>().birthdate()};
}

}//namespace Fred::Identity::Impl::Unwrapper
}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred
