/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef API_TYPE_SETTER_IMPL_HH_B18F55FE3C60E0911E5D04C20CD69F9C//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define API_TYPE_SETTER_IMPL_HH_B18F55FE3C60E0911E5D04C20CD69F9C

#include "src/impl/api_type_setter.hh"

#include <google/protobuf/repeated_field.h>

namespace Fred {
namespace Identity {
namespace Impl {

template <typename WrapperType>
template <typename ApiType>
ApiTypeSetter<WrapperType>::ApiTypeSetter(ApiType* ptr) noexcept
    : api_data_{reinterpret_cast<void*>(ptr)}
{
    static_assert(std::is_same<Wrapper::GetApiT<WrapperType>, ApiType>::value, "ApiType does not correspond to WrapperType");
}

template <typename WrapperType>
template <typename ApiType>
ApiType* ApiTypeSetter<WrapperType>::get() const noexcept
{
    static_assert(std::is_same<Wrapper::GetApiT<WrapperType>, ApiType>::value, "ApiType does not correspond to WrapperType");
    return reinterpret_cast<ApiType*>(api_data_);
}

template <typename T>
const ApiRepeatedTypeSetter<T>& ApiRepeatedTypeSetter<T>::reserve(int new_size) const
{
    this->ApiData::template get<::google::protobuf::RepeatedPtrField<Wrapper::GetApiT<T>>>()->Reserve(new_size);
    return *this;
}

template <typename T>
T ApiRepeatedTypeSetter<T>::add() const
{
    return T{this->ApiData::template get<::google::protobuf::RepeatedPtrField<Wrapper::GetApiT<T>>>()->Add()};
}

template <typename T>
int ApiRepeatedTypeSetter<T>::capacity() const
{
    return this->ApiData::template get<::google::protobuf::RepeatedPtrField<Wrapper::GetApiT<T>>>()->Capacity();
}

template <typename T>
bool ApiRepeatedTypeSetter<T>::empty() const
{
    return this->ApiData::template get<::google::protobuf::RepeatedPtrField<Wrapper::GetApiT<T>>>()->empty();
}

template <typename T>
int ApiRepeatedTypeSetter<T>::size() const
{
    return this->ApiData::template get<::google::protobuf::RepeatedPtrField<Wrapper::GetApiT<T>>>()->size();
}

}//namespace Fred::Identity::Impl
}//namespace Fred::Identity
}//namespace Fred

#endif//API_TYPE_SETTER_IMPL_HH_B18F55FE3C60E0911E5D04C20CD69F9C
